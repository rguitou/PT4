To add a migration :
Add-Migration Name -ConnectionStringName ConnectionDB
To update the database :
Update-Database -ConnectionStringName ConnectionDB