﻿using System.Drawing;
using System.Net.Mime;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    public class HelperTest : BaseTest
    {
        [TestMethod]
        public void HelperGenerateGraphiqueBarcodeTest()
        {
            Image image = Model.HelperGetGraphicBarcode("1111111111111");
            Assert.IsNotNull(image);
        }
    }
}