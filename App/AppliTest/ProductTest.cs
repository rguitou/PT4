﻿using System.Collections.Generic;
using Appli.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AppliTest
{
    [TestClass]
    public class ProductTest : BaseTest
    {
        static Product _sampleProductAdd = new Product
        {
            Wording = "testAdd",
            IsActive = true,
        };

        static Product _sampleProductDel = new Product
        {
            Wording = "testDel",
            IsActive = true,
        };

        [TestMethod]
        public void CreateProductTest()
        {
            int id = Model.CreateProduct(_sampleProductAdd);
            Assert.IsTrue(Model.GetProducts(false).Exists(pdt => pdt.Wording == _sampleProductAdd.Wording));
            //since product wording is unique, this can be used to confirm correct insertion
        }

        [TestMethod]
        public void DeleteProductTest()
        {
            Model.CreateProduct(_sampleProductDel);
            //add a product
            Product added = Model.GetProducts(false, false).Find(pdt => pdt.Wording == _sampleProductDel.Wording);
            //get it from the database

            Model.UpdateProduct(_sampleProductDel);
            Assert.IsTrue(Model.GetProducts(false).Exists(pdt => pdt.Wording == _sampleProductDel.Wording));
            //confirmation of correct insertion

            Model.DeleteProduct(added);
            Assert.IsFalse(added.IsActive);
        }

        [TestMethod]
        public void GetListProductsTest()
        {
            List<Product> products = Model.GetProducts();
            Assert.AreNotEqual(null, products);
        }
    }
}
