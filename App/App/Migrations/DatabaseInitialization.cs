﻿using Appli.Model;
using Appli.Model.Class;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Appli.Migrations
{
    public static class DatabaseInitialization
    {
        public static void FillDatabase(AppliContext context, bool test)
        {
            Product article1;
            Product article2;
            Packet packet;
            List<Customer> customers = new List<Customer>();
            List<Order> orders = new List<Order>();
            List<SupportType> supportTypes = new List<SupportType>();
            List<Supplier> suppliers = new List<Supplier>();

            //Add depositories
            List<Depository> depositories = new List<Depository>();
            if (!context.Depositories.Any())
            {
                depositories.Add(context.Depositories.Add(new Depository() { Address = "123 avenue des dépots", Postcode = "33000", City = "Bordeaux", Name = "Depot 1" }));
                depositories.Add(context.Depositories.Add(new Depository() { Address = "456 avenue des dépots", Postcode = "31000", City = "Toulouse", Name = "Depot 2" }));
                depositories.Add(context.Depositories.Add(new Depository() { Address = "789 avenue des dépots", Postcode = "45000", City = "Orléans", Name = "Depot 3" }));
            }
            else
                depositories = context.Depositories.ToList();

            //Add support types
            if (!context.Supplier.Any())
            {
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette classique", Height = 150, Length = 100, Width = 50 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette Europe", Height = 200, Length = 120, Width = 80 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette USA", Height = 200, Length = 100, Width = 120 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Hors gabarit", Height = int.MaxValue, Length = int.MaxValue, Width = int.MaxValue }));
            }
            else
                supportTypes = context.SupportTypes.ToList();

            if (!test)
            {
                if (!context.Products.Any())
                {
                    article1 = context.Products.Add(new Product() { Wording = "Mon premier article", IsActive = true });
                    article2 = context.Products.Add(new Product() { Wording = "Mon 3ème article", IsActive = true });
                    context.Products.Add(new Product() { Wording = "Mon second article", IsActive = true, });
                    packet = (Packet)context.Products.Add(new Packet() { Wording = "Mon premier colis", IsActive = true, });
                    context.PacketArticles.Add(new PacketArticle() { Article = article1, Packet = packet, Quantity = 2 });
                }
                else
                {
                    article1 = context.Products.OfType<Product>().First();
                    article2 = context.Products.OfType<Product>().ToList()[1];
                    packet = context.Products.OfType<Packet>().First();
                }




                //Add suppliers
                if (!context.Supplier.Any())
                {
                    suppliers.Add(context.Supplier.Add(new Supplier() { Name = "Mon premier fournisseur", Address = "456 avenue des fournisseurs", Postcode = "33000", City = "Bordeaux CEDEX" }));
                    suppliers.Add(context.Supplier.Add(new Supplier() { Name = "Mon second fournisseur", Address = "789 avenue des fournisseurs", Postcode = "75000", City = "Paris CEDEX" }));
                }
                else
                    suppliers = context.Supplier.ToList();

                //Add customers
                if (!context.Customers.Any())
                {
                    customers.Add(context.Customers.Add(new Customer() { LastName = "DOE", FirstName = "John" }));
                    customers.Add(context.Customers.Add(new Customer() { LastName = "DOE", FirstName = "Jane" }));
                }
                else
                    customers = context.Customers.ToList();

                //Add orders
                if (!context.Orders.Any())
                {
                    orders.Add(context.Orders.Add(new CustomerOrder() { Barcode = "1000000000001", Customer = customers.First(), Address = "123 rue du client", Postcode = "75000", City = "Paris", OrderDate = new DateTime(2018, 1, 1) }));
                    orders.Add(context.Orders.Add(new SupplierOrder() { Barcode = "1000000000002", Supplier = suppliers.First(), Depository = depositories.First(), OrderDate = new DateTime(2018, 2, 1) }));
                }
                else
                    orders = context.Orders.ToList();

                if (!context.OrderProducts.Any())
                {
                    context.OrderProducts.Add(new OrderProduct() { Order = orders.OfType<CustomerOrder>().First(), Product = packet });
                    context.OrderProducts.Add(new OrderProduct() { Order = orders.OfType<SupplierOrder>().First(), Product = article1 });
                }

                List<string> barcodes = new List<string>();

                foreach (Depository depository in depositories)
                {
                    for (int store = 0; store < 3; store++)
                    {
                        Store newStore;
                        if (!context.Stores.Any())
                            newStore = context.Stores.Add(new Store() { Depository = depository, Name = "Magasin " + store, Lenght = 10000, Width = 5000, });
                        else
                            newStore = context.Stores.ToList()[store];
                        for (int path = 0; path < 10; path++)
                        {
                            Path newPath;
                            if (!context.Paths.Any())
                                newPath = context.Paths.Add(new Path() { Lenght = 4500, Name = Model.Model.HelperAlphabetLetter(path), PathWidth = 250, StorageWidth = 100, Store = newStore, Unilateral = false });
                            else
                                newPath = context.Paths.ToList()[path];
                            for (int row = 1; row <= 6; row++)
                            {
                                Row newRow;
                                if (!context.Rows.Any())
                                    newRow = context.Rows.Add(new Row() { Name = row, Path = newPath, Width = 100 });
                                else
                                    newRow = context.Rows.ToList()[row];
                                for (int position = 0; position < 2; position++)
                                {
                                    Position newPosition;
                                    if (!context.Rows.Any())
                                    {
                                        Random random = new Random();
                                        bool unique = false;
                                        string barcode = "";
                                        do
                                        {
                                            barcode = random.Next(100000, 999999).ToString() + random.Next(1000000, 9999999).ToString();
                                            unique = !barcodes.Contains(barcode);
                                        } while (!unique);
                                        barcodes.Add(barcode);
                                        newPosition = context.Positions.Add(new Position() { Barcode = barcode, Name = Model.Model.HelperAlphabetLetter(position), Row = newRow, SupportType = supportTypes.First() });
                                    }
                                    else
                                        newPosition = context.Positions.ToList()[position];
                                    if (!context.Floors.Any())
                                    {

                                        for (int floar = 0; floar < 3; floar++)
                                        {
                                            Random random = new Random();
                                            bool unique;
                                            string barcode = "";
                                            do
                                            {
                                                barcode = random.Next(100000, 999999).ToString() + random.Next(1000000, 9999999).ToString();
                                                unique = !barcodes.Contains(barcode);
                                            } while (!unique);
                                            barcodes.Add(barcode);
                                            Floor newFloor;
                                            if (floar == 0)
                                                newFloor = context.Floors.Add(new Floor() { Position = newPosition, AddressType = AddressType.Picking, Value = floar });
                                            else
                                            {
                                                newFloor = context.Floors.Add(new Floor() { Position = newPosition, AddressType = AddressType.Stock, Value = floar });
                                                if (path == 2)
                                                    context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = article2, Quantity = floar, LotNumber = "123", SerialNumber = "456", CustomsNumber = "789", ExpirationDate = DateTime.Now.AddMonths(1), ReceptionDate = DateTime.Now, Barcode = barcode });
                                            }
                                            if (path == 1)
                                            {

                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = article1, Quantity = floar, LotNumber = "123", SerialNumber = "456", CustomsNumber = "789", ExpirationDate = DateTime.Now.AddMonths(1), ReceptionDate = DateTime.Now, Barcode = barcode });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        public static void FillDatabaseDemo(AppliContext context)
        {
            // 15 Products
            Product lamp = null;
            Product table = null;
            Product chair = null;
            Product guitar = null;
            Product violin = null;
            Product vacuum = null;
            Product console = null;
            Product printer = null;
            Product tv = null;
            Product coffeeMachine = null;
            Product suitcase = null;
            Product nappy = null;
            Product towels = null;
            Product barbecue = null;
            Product projector = null;
            Product banane = null;

            // 5 Packets
            Packet homePacket = null;
            Packet instrumentPacket = null;
            Packet multimediaPacket = null;
            Packet eatPacket = null;
            Packet babyPacket = null;

            int IkeaId;
            int ThomannId;
            int BoulangerId;
            int LeclercId;

            // CustomersName
            Customer Julien = null;
            Customer Teddy = null;
            Customer GaetanV = null;
            Customer GaetanB = null;

            // Customers
            List<Customer> customers = new List<Customer>();

            // Orders
            List<Order> orders = new List<Order>();

            // Support types
            List<SupportType> supportTypes = new List<SupportType>();

            // Suppliers
            List<Supplier> suppliers = new List<Supplier>();

            // Add Depositories
            List<Depository> depositories = new List<Depository>();
            if (!context.Depositories.Any())
            {
                depositories.Add(context.Depositories.Add(new Depository() { Address = "123 avenue des dépots", Postcode = "33000", City = "Bordeaux", Name = "Depot A" }));
                depositories.Add(context.Depositories.Add(new Depository() { Address = "456 avenue des dépots", Postcode = "33000", City = "Bordeaux", Name = "Depot B" }));
                depositories.Add(context.Depositories.Add(new Depository() { Address = "789 avenue des dépots", Postcode = "33000", City = "Bordeaux", Name = "Depot C" }));
            }
            else
                depositories = context.Depositories.ToList();

            //  Add Support types
            if (!context.Supplier.Any())
            {
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette classique", Height = 150, Length = 100, Width = 50 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette Europe", Height = 200, Length = 120, Width = 80 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Palette USA", Height = 200, Length = 100, Width = 120 }));
                supportTypes.Add(context.SupportTypes.Add(new SupportType() { Name = "Hors gabarit", Height = int.MaxValue, Length = int.MaxValue, Width = int.MaxValue }));
            }
            else
                supportTypes = context.SupportTypes.ToList();

            if (!context.Products.Any())
            {
                // Initialization Products
                banane = context.Products.Add(new Product() { Wording = "Banane du Togo", IsActive = false });
                lamp = context.Products.Add(new Product() { Wording = "Lampe à économie d'énergie", IsActive = true });
                table = context.Products.Add(new Product() { Wording = "Table en bois d'accacia", IsActive = true });
                chair = context.Products.Add(new Product() { Wording = "Chaise avec dossier en cuir", IsActive = true });
                guitar = context.Products.Add(new Product() { Wording = "Guitare Ibanez à 7 cordes", IsActive = true });
                violin = context.Products.Add(new Product() { Wording = "Violon Stratovarius", IsActive = true });
                vacuum = context.Products.Add(new Product() { Wording = "Aspirateur sans sac", IsActive = true });
                console = context.Products.Add(new Product() { Wording = "PS4 Pro 1To", IsActive = true });
                printer = context.Products.Add(new Product() { Wording = "Imprimante Laser", IsActive = true });
                tv = context.Products.Add(new Product() { Wording = "Télé 4K Incurvée", IsActive = true });
                coffeeMachine = context.Products.Add(new Product() { Wording = "Machine à café Senseo", IsActive = true });
                suitcase = context.Products.Add(new Product() { Wording = "Valide 10 Litres", IsActive = true });
                nappy = context.Products.Add(new Product() { Wording = "Couches Pampers", IsActive = true });
                towels = context.Products.Add(new Product() { Wording = "Serviettes de bain x3", IsActive = true });
                barbecue = context.Products.Add(new Product() { Wording = "Barbecue au gaz", IsActive = true });
                projector = context.Products.Add(new Product() { Wording = "Projecteur Monochromatique", IsActive = true });
                // Initialization Packet
                homePacket = (Packet)context.Products.Add(new Packet() { Wording = "Colis de mobiliers", IsActive = true, });
                context.PacketArticles.Add(new PacketArticle() { Article = lamp, Packet = homePacket, Quantity = 2 });
                context.PacketArticles.Add(new PacketArticle() { Article = table, Packet = homePacket, Quantity = 1 });
                context.PacketArticles.Add(new PacketArticle() { Article = chair, Packet = homePacket, Quantity = 5 });

                instrumentPacket = (Packet)context.Products.Add(new Packet() { Wording = "Colis d'instruments", IsActive = true, });
                context.PacketArticles.Add(new PacketArticle() { Article = guitar, Packet = instrumentPacket, Quantity = 1 });
                context.PacketArticles.Add(new PacketArticle() { Article = violin, Packet = instrumentPacket, Quantity = 1 });

                multimediaPacket = (Packet)context.Products.Add(new Packet() { Wording = "Colis d'objets multimédia", IsActive = true, });
                context.PacketArticles.Add(new PacketArticle() { Article = console, Packet = multimediaPacket, Quantity = 1 });
                context.PacketArticles.Add(new PacketArticle() { Article = printer, Packet = multimediaPacket, Quantity = 1 });
                context.PacketArticles.Add(new PacketArticle() { Article = tv, Packet = multimediaPacket, Quantity = 2 });

                eatPacket = (Packet)context.Products.Add(new Packet() { Wording = "Colis Outils Cuisine", IsActive = true, });
                context.PacketArticles.Add(new PacketArticle() { Article = barbecue, Packet = eatPacket, Quantity = 1 });
                context.PacketArticles.Add(new PacketArticle() { Article = coffeeMachine, Packet = eatPacket, Quantity = 1 });

                babyPacket = (Packet)context.Products.Add(new Packet() { Wording = "Colis pour bébé", IsActive = true, });
                context.PacketArticles.Add(new PacketArticle() { Article = nappy, Packet = babyPacket, Quantity = 5 });;
                context.SaveChanges();
            }

            //Add suppliers
            if (!context.Supplier.Any())
            {
                Supplier Ikea = context.Supplier.Add(new Supplier() { Name = "Ikea", Address = "456 avenue des fournisseurs", Postcode = "33000", City = "Bordeaux CEDEX" });
                Supplier Thomann = context.Supplier.Add(new Supplier() { Name = "Thomann", Address = "789 avenue des fournisseurs", Postcode = "75000", City = "Paris CEDEX" });
                Supplier Boulanger = context.Supplier.Add(new Supplier() { Name = "Boulanger", Address = "789 avenue des fournisseurs", Postcode = "75000", City = "Paris CEDEX" });
                Supplier Leclerc = context.Supplier.Add(new Supplier() { Name = "Leclerc", Address = "789 avenue des fournisseurs", Postcode = "75000", City = "Paris CEDEX" });
                suppliers.Add(Ikea);
                suppliers.Add(Thomann);
                suppliers.Add(Boulanger);
                suppliers.Add(Leclerc);
                context.SaveChanges();
                IkeaId = Ikea.Id;
                ThomannId = Thomann.Id;
                BoulangerId = Boulanger.Id;
                LeclercId = Leclerc.Id;
            }
            else
                suppliers = context.Supplier.ToList();

            //Add customers
            if (!context.Customers.Any())
            {
                Julien = new Customer() { LastName = "Germanaud", FirstName = "Julien" };
;               Teddy = new Customer() { LastName = "Barbessou", FirstName = "Teddy" };
                GaetanV = new Customer() { LastName = "Viola", FirstName = "Gaetan" };
                GaetanB = new Customer() { LastName = "Basile", FirstName = "Gaetan" };
                customers.Add(context.Customers.Add(Julien));
                customers.Add(context.Customers.Add(Teddy));
                customers.Add(context.Customers.Add(GaetanV));
                customers.Add(context.Customers.Add(GaetanB));
            }
            else
                customers = context.Customers.ToList();

            //Add orders
            if (!context.Orders.Any())
            {
                // Commandes clients
                orders.Add(context.Orders.Add(new CustomerOrder() { Barcode = "1000000000000", Customer = Julien, Address = "123 rue du pro du diapo", Postcode = "75000", City = "Paris", OrderDate = new DateTime(2018, 1, 1) }));
                orders.Add(context.Orders.Add(new CustomerOrder() { Barcode = "1000000000002", Customer = Teddy, Address = "123 rue du chef de projet", Postcode = "33000", City = "Bordeaux", OrderDate = new DateTime(2017, 1, 1) }));
                orders.Add(context.Orders.Add(new CustomerOrder() { Barcode = "1000000000003", Customer = GaetanV, Address = "123 rue du super dev", Postcode ="24100", City = "Bergerac", OrderDate = new DateTime(2015, 1, 1) }));
                orders.Add(context.Orders.Add(new CustomerOrder() { Barcode = "1000000000004", Customer = GaetanB, Address = "123 rue du dormeur", Postcode = "64000", City = "Pau", OrderDate = new DateTime(2014, 1, 1) }));
                // 
                context.OrderProducts.Add(new OrderProduct() { Order = orders.First(), Product = banane, Quantity=150});
                orders.Remove(orders.First());
                context.OrderProducts.Add(new OrderProduct() { Order = orders.First(), Product = instrumentPacket, Quantity = 1 });
                orders.Remove(orders.First());
                context.OrderProducts.Add(new OrderProduct() { Order = orders.First(), Product = towels, Quantity = 2 });
                orders.Remove(orders.First());
                context.OrderProducts.Add(new OrderProduct() { Order = orders.First(), Product = violin, Quantity = 12 });
                orders.Remove(orders.First());
                // Commande fournisseur
                orders.Add(context.Orders.Add(new SupplierOrder() { Barcode = "2000000000001", Supplier = suppliers.First(), Depository = depositories.First(), OrderDate = new DateTime(2018, 2, 1) }));
                orders.Add(context.Orders.Add(new SupplierOrder() { Barcode = "2000000000002", Supplier = suppliers.First(), Depository = depositories.First(), OrderDate = new DateTime(2018, 2, 1) }));
            }

            List<string> barcodes = new List<string>();

            foreach (Depository depository in depositories)
            {
                // 3 stores for one depository
                for (int store = 0; store < 3; store++)
                {
                    Store newStore;
                    if (!context.Stores.Any())
                        newStore = context.Stores.Add(new Store() { Depository = depository, Name = "Magasin " + store, Lenght = 10000, Width = 5000, });
                    else
                        newStore = context.Stores.ToList()[store];
                    // 10 paths for one store
                    for (int path = 0; path < 10; path++)
                    {
                        Path newPath;
                        if (!context.Paths.Any())
                            newPath = context.Paths.Add(new Path() { Lenght = 4500, Name = Model.Model.HelperAlphabetLetter(path), PathWidth = 250, StorageWidth = 100, Store = newStore, Unilateral = false });
                        else
                            newPath = context.Paths.ToList()[path];
                        // 5 rows for one path
                        for (int row = 1; row <= 6; row++)
                        {
                            Row newRow;
                            if (!context.Rows.Any())
                                newRow = context.Rows.Add(new Row() { Name = row, Path = newPath, Width = 100 });
                            else
                                newRow = context.Rows.ToList()[row];
                            // 
                            for (int position = 0; position < 3; position++)
                            {
                                Position newPosition;
                                if (!context.Rows.Any())
                                {
                                    Random random = new Random();
                                    bool unique = false;
                                    string barcode = "";
                                    do
                                    {
                                        barcode = random.Next(100000, 999999).ToString() + random.Next(1000000, 9999999).ToString();
                                        unique = !barcodes.Contains(barcode);
                                    } while (!unique);
                                    barcodes.Add(barcode);
                                    newPosition = context.Positions.Add(new Position() { Barcode = barcode, Name = Model.Model.HelperAlphabetLetter(position), Row = newRow, SupportType = supportTypes.First() });
                                }
                                else
                                    newPosition = context.Positions.ToList()[position];
                                if (!context.Floors.Any())
                                {

                                    for (int floar = 0; floar < 3; floar++)
                                    {
                                        Random random = new Random();
                                        bool unique;
                                        string barcode = "";
                                        do
                                        {
                                            barcode = random.Next(100000, 999999).ToString() + random.Next(1000000, 9999999).ToString();
                                            unique = !barcodes.Contains(barcode);
                                        } while (!unique);
                                        barcodes.Add(barcode);
                                        Floor newFloor;
                                        // The floors at the bottom are designed for the picking
                                        if (floar == 0)
                                        {
                                            newFloor = context.Floors.Add(new Floor() { Position = newPosition, AddressType = AddressType.Picking, Value = floar });
                                            if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "A" && newRow.Name == 1 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = violin, Quantity = floar, LotNumber = "123", SerialNumber = "456", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(24), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "J" && newRow.Name == 5 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = guitar, Quantity = floar, LotNumber = "124", SerialNumber = "457", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(24), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 3" && newPath.Name == "B" && newRow.Name == 5 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = eatPacket, Quantity = floar, LotNumber = "125", SerialNumber = "458", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(12), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 0" && newPath.Name == "C" && newRow.Name == 2 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = multimediaPacket, Quantity = floar, LotNumber = "1236", SerialNumber = "459", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(36), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 0" && newPath.Name == "I" && newRow.Name == 4 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = instrumentPacket, Quantity = floar, LotNumber = "127", SerialNumber = "460", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(2), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 1" && newPath.Name == "E" && newRow.Name == 5 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = lamp, Quantity = floar, LotNumber = "128", SerialNumber = "461", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(1), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 1" && newPath.Name == "F" && newRow.Name == 3 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = chair, Quantity = floar, LotNumber = "129", SerialNumber = "462", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(3), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 2" && newPath.Name == "C" && newRow.Name == 1 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = table, Quantity = floar, LotNumber = "130", SerialNumber = "463", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(48), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "C" && newRow.Name == 1 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = vacuum, Quantity = floar, LotNumber = "131", SerialNumber = "464", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(21), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "F" && newRow.Name == 2 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = console, Quantity = 2, LotNumber = "132", SerialNumber = "465", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(22), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "G" && newRow.Name == 3 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = tv, Quantity = floar, LotNumber = "133", SerialNumber = "466", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(11), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "A" && newRow.Name == 4 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = printer, Quantity = floar, LotNumber = "134", SerialNumber = "467", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(13), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "I" && newRow.Name == 5 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = coffeeMachine, Quantity = floar, LotNumber = "135", SerialNumber = "468", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(15), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot C" && newStore.Name == "Magasin 2" && newPath.Name == "H" && newRow.Name == 5 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = suitcase, Quantity = floar, LotNumber = "136", SerialNumber = "469", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(2), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot C" && newStore.Name == "Magasin 1" && newPath.Name == "H" && newRow.Name == 5 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = nappy, Quantity = floar, LotNumber = "137", SerialNumber = "470", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(3), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot C" && newStore.Name == "Magasin 2" && newPath.Name == "F" && newRow.Name == 3 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = towels, Quantity = floar, LotNumber = "138", SerialNumber = "471", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(1), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                        }
                                        // The floors at the top are designed for the stock
                                        else
                                        {
                                            newFloor = context.Floors.Add(new Floor() { Position = newPosition, AddressType = AddressType.Stock, Value = floar });
                                            if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "A" && newRow.Name == 1 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = babyPacket, Quantity = floar, LotNumber = "138", SerialNumber = "472", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(1), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 1" && newPath.Name == "J" && newRow.Name == 5 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = homePacket, Quantity = floar, LotNumber = "139", SerialNumber = "473", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(2), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 2" && newPath.Name == "B" && newRow.Name == 5 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = violin, Quantity = floar, LotNumber = "140", SerialNumber = "474", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(3), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 0" && newPath.Name == "C" && newRow.Name == 2 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = table, Quantity = floar, LotNumber = "141", SerialNumber = "475", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(4), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 0" && newPath.Name == "I" && newRow.Name == 4 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = chair, Quantity = floar, LotNumber = "142", SerialNumber = "476", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(5), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 2" && newPath.Name == "J" && newRow.Name == 5 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = lamp, Quantity = floar, LotNumber = "143", SerialNumber = "477", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(6), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 1" && newPath.Name == "J" && newRow.Name == 3 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = printer, Quantity = floar, LotNumber = "144", SerialNumber = "478", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(7), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot B" && newStore.Name == "Magasin 2" && newPath.Name == "C" && newRow.Name == 1 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = tv, Quantity = floar, LotNumber = "145", SerialNumber = "479", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(8), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "C" && newRow.Name == 1 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = coffeeMachine, Quantity = floar, LotNumber = "146", SerialNumber = "480", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(9), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "F" && newRow.Name == 2 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = suitcase, Quantity = floar, LotNumber = "147", SerialNumber = "481", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(10), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 1" && newPath.Name == "F" && newRow.Name == 3 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = nappy, Quantity = floar, LotNumber = "148", SerialNumber = "482", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(11), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 0" && newPath.Name == "G" && newRow.Name == 4 && newPosition.Name == "C")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = towels, Quantity = floar, LotNumber = "149", SerialNumber = "483", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(12), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot A" && newStore.Name == "Magasin 2" && newPath.Name == "I" && newRow.Name == 5 && newPosition.Name == "B")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = barbecue, Quantity = floar, LotNumber = "150", SerialNumber = "484", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(13), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                            else if (depository.Name == "Depot C" && newStore.Name == "Magasin 1" && newPath.Name == "H" && newRow.Name == 5 && newPosition.Name == "A")
                                            {
                                                context.ProductFloors.Add(new ProductFloor() { Floor = newFloor, Product = projector, Quantity = floar, LotNumber = "151", SerialNumber = "485", CustomsNumber = "1111111111111", ExpirationDate = DateTime.Now.AddMonths(14), ReceptionDate = DateTime.Now, Barcode = barcode, Stored = true });
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
