﻿using Appli.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Appli.Model.Class;
using Appli.Model.Class.UI;
using MahApps.Metro.Controls;

namespace Appli
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public List<AppliUserControl> UserControls;
        public MainWindow()
        {
            UserControls = new List<AppliUserControl>();
            Globals.Model = new Model.Model();
            InitializeComponent();
            switch (Parameters.LastUserControl)
            {
                case "Appli.Views.Reception":
                    CcPrincipal.Content = new Reception();
                    MenuListBox.SelectedIndex = 0;
                    break;
                case "Appli.Views.Storage":
                    CcPrincipal.Content = new Storage();
                    MenuListBox.SelectedIndex = 1;
                    break;
                case "Appli.Views.Articles":
                    CcPrincipal.Content = new Articles();
                    MenuListBox.SelectedIndex = 2;
                    break;
                case "Appli.Views.InternalsMovements":
                    CcPrincipal.Content = new InternalsMovements();
                    MenuListBox.SelectedIndex = 3;
                    break;
                case "Appli.Views.Preparation":
                    CcPrincipal.Content = new Preparation();
                    MenuListBox.SelectedIndex = 4;
                    break;
                case "Appli.Views.Configurations":
                    CcPrincipal.Content = new Configurations();
                    MenuListBox.SelectedIndex = 5;
                    break;
                default:
                    CcPrincipal.Content = new Reception();
                    break;
            }
        }



        private void MenuListBox_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //until we had a StaysOpen glag to Drawer, this will help with scroll bars
            var dependencyObject = Mouse.Captured as DependencyObject;
            while (dependencyObject != null)
            {
                if (dependencyObject is ScrollBar) return;
                dependencyObject = VisualTreeHelper.GetParent(dependencyObject);
            }

            MenuToggleButton.IsChecked = false;
        }

        /// <summary>
        /// Action lorsqu'on sélectionne le listBoxItem stockage du menu principal. Enregistre le contenu de la fenêtre principale dans une liste pour pouvoir le réutiliser plus tard. 
        /// Change le contenu de la fenêtre pour un userControle Articles.
        /// </summary>
        /// <param name="sender">ListBoxItem</param>
        /// <param name="e"></param>
        private void LbiArticles_Selected(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
            {
                Articles articles = (Articles)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.Articles"));
                if (articles == null)
                {
                    AppliUserControl userControl = new Articles();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = articles;
            }
        }

        /// <summary>
        /// Storage
        /// </summary>
        /// <param name="sender">ListBoxItem</param>
        /// <param name="e"></param>
        private void LbiStorage_Selected(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
            {
                Storage storages = (Storage)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.Storage"));
                if (storages == null)

                {
                    AppliUserControl userControl = new Storage();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = storages;
            }
        }


        /// <summary>
        /// Action lorsqu'on sélectionne le listBoxItem réception du menu principal. Enregistre le contenu de la fenêtre principale dans une liste pour pouvoir le réutiliser plus tard.
        /// Change le contenu de la fenêtre pour un userControle Reception.
        /// </summary>
        /// <param name="sender">ListBoxItem</param>
        /// <param name="e"></param>
        private void LbiReception_Selected(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
            {
                Reception reception = (Reception)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.Reception"));
                if (reception == null)
                {
                    AppliUserControl userControl = new Reception();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = reception;
            }
        }

        /// <summary>
        /// Action lorsqu'on sélectionne le listBoxItem mouvement interne du menu principal. Enregistre le contenu de la fenêtre principale dans une liste pour pouvoir le réutiliser plus tard.
        /// Change le contenu de la fenêtre pour un userControle MouvementInterne.
        /// </summary>
        /// <param name="sender">ListBoxItem</param>
        /// <param name="e"></param>
        private void LbiInternMovement_Selected(object sender, RoutedEventArgs e)
        {

            if (IsInitialized)
            {
                InternalsMovements intMov = (InternalsMovements)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.InternalsMovements"));
                if (intMov == null)
                {
                    AppliUserControl userControl = new InternalsMovements();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = intMov;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LbiConfigs_Selected(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
            {

                Configurations settings = (Configurations)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.Configurations"));
                if (settings == null)
                {
                    AppliUserControl userControl = new Configurations();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = settings;
            }
        }

        /// <summary>
        /// Action lorsqu'on sélectionne le listBoxItem préparation du menu principal. Enregistre le contenu de la fenêtre principale dans une liste pour pouvoir le réutiliser plus tard.
        /// Change le contenu de la fenêtre pour un userControle Preparation.
        /// </summary>
        /// <param name="sender">ListBoxItem</param>
        /// <param name="e"></param>
        private void LbiPreparation_Selected(object sender, RoutedEventArgs e)
        {
            if (IsInitialized)
            {
                Preparation intMov = (Preparation)UserControls.SingleOrDefault(x => x.GetType() == Type.GetType("Appli.Views.Preparation"));
                if (intMov == null)
                {
                    AppliUserControl userControl = new Preparation();
                    CcPrincipal.Content = userControl;
                    UserControls.Add(userControl);
                }
                else
                    CcPrincipal.Content = intMov;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Parameters.LastUserControl = CcPrincipal.Content.GetType().ToString();
            Parameters.Save();
        }
    }
}
