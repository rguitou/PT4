﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};DepositoryId={DepositoryId};Width={Width};Lenght={Lenght}")]
    public class Store
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The name of the store
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// The parent depository
        /// </summary>
        [Required]
        [ForeignKey(nameof(Depository))]
        public int DepositoryId { get; set; }
        [ForeignKey(nameof(DepositoryId))]
        public Depository Depository { get; set; }

        /// <summary>
        /// The width of the store in centimeters
        /// </summary>
        [Required]
        [Range(0,int.MaxValue)]
        public int Width { get; set; }

        /// <summary>
        /// The lenght of the store in centimeters
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Lenght { get; set; }
    }
}
