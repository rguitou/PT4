﻿namespace Appli.Model.Class
{
    public static class Parameters
    {
        /// <summary>
        /// Save the last user control opened to open it at the next start of the application.
        /// </summary>
        public static string LastUserControl
        {
            get { return Properties.Settings.Default.LastUserControl; }
            set { Properties.Settings.Default.LastUserControl = value; }
        }

        public static bool ReceptionPrintPreference
        {
            get { return Properties.Settings.Default.ReceptionPrintPreference; }
            set { Properties.Settings.Default.ReceptionPrintPreference = value; }
        }

        /// <summary>
        /// Save the parameters
        /// </summary>
        public static void Save()
        {
            Properties.Settings.Default.Save();
        }
    }
}
