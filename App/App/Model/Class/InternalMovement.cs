﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Appli.Model.Class
{
    public class InternalMovement
    {
        [Required]
        public Floor StartingFloor;
        [Required]
        public Floor FinalFloor;
        [Required]
        public Product MovedProduct { get; set; }
        [Required]
        public int Quantity { get; set; }
        public bool Validate { get; set; }
        public string StartingFloorToString =>
                StartingFloor.Position.Row.Path.Store.Depository.Name + "/" +
                StartingFloor.Position.Row.Path.Store.Name + "/" +
                StartingFloor.Position.Row.Path.Name + "/" +
                StartingFloor.Position.Row.Name + "/" +
                StartingFloor.Position.Name + "/" +
                StartingFloor.Id;

        public string FinalFloorToString =>
                FinalFloor.Position.Row.Path.Store.Depository.Name + "/" +
                FinalFloor.Position.Row.Path.Store.Name + "/" +
                FinalFloor.Position.Row.Path.Name + "/" +
                FinalFloor.Position.Row.Name + "/" +
                FinalFloor.Position.Name + "/" +
                FinalFloor.Id;

        public ProductFloor ProductFloorConcerned;
    }
}
