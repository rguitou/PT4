﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Appli.Model.Class.UI
{
    public class AppliUserControl: UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public bool ThereAreErrors
        {
            get { return Errors?.Length > 0; }
        }

        private string _errors;

        public string Errors
        {
            get { return _errors; }
            set
            {
                if (value == _errors) return;
                _errors = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(ThereAreErrors));
            }
        }

        /// <summary>
        /// Control that only number are entered
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void PreviewTextInputOnlyNumbers(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !OnlyNumber(e.Text);
        }

        private static bool OnlyNumber(string text)
        {
            Regex regex = new Regex("[^0-9]+"); //regex that matches disallowed text
            return !regex.IsMatch(text);
        }

        // Use the DataObject.Pasting Handler 
        public void TextBoxPastingOnlyNumbers(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                if (!OnlyNumber(text))
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }

        public static bool IsFormatValid(string text)
        {
            long result;
            if (Int64.TryParse(text, out result))
            {
                long number = Int64.Parse(text);
                if (Math.Ceiling(Math.Log10(number)) == 13)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool FilterValidation(string label, string text, TextBlock error)
        {
            if (text != "" && !IsFormatValid(text))
            {
                error.Text = label + " est non conforme";
                return false;
            }

            return true;
        }

    }
}
