﻿using Appli.Model.Class;
using Appli.Model.Class.Exception;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using Zen.Barcode;

namespace Appli.Model
{
    public class Model
    {

        #region Const
        private const AddressType DefaultAddressType = AddressType.Picking;
        private const int MaxLimitOfStoreInDepository = 10;
        private const int MaxLimitOfPathInStore = 19;
        private const int MaxLimitOfPositionInRow = 3;
        private const int MaxLimitOfAddressInStore = 10000;

        #endregion
        public Model()
        {
            _contextDb = new AppliContext();
        }
        private AppliContext _contextDb;

        /// <summary>
        /// Registers a product for storing in the database.
        /// </summary>
        /// <param name="toStore">The product to be stored</param>
        /// <param name="targetDep">The depository in which to store</param>
        /// <param name="barcode">The barcode whick is generated</param>
        /// <param name="traceAbilityInformation">Optionnal traceability information</param>
        /// <param name="quantity">The quantity of items to store</param>
        /// <returns></returns>
        public Address ReceiveProduct(Product toStore, Depository targetDep, out string barcode, ProductFloor traceAbilityInformation = null, int quantity = 1)
        {
            if (!_contextDb.Products.AsNoTracking().Any(pdt => pdt.Id == toStore.Id))
            {
                throw new ShowableException("Le produit spécifié n'existe pas dans la base de données.");
            }

            if (!_contextDb.Depositories.AsNoTracking().Any(dep => dep.Id == targetDep.Id))
            {
                throw new ShowableException("Le dépôt spécifié n'existe pas dans la bose de données.");
            }


            Floor storageLocation;

            ProductFloor existingStorage = _contextDb.ProductFloors.AsNoTracking().Include(pdtFlr => pdtFlr.Floor.Position.Row.Path.Store.Depository)
                .FirstOrDefault(pdtFlr => pdtFlr.Product.Id == toStore.Id
                && pdtFlr.Floor.AddressType == AddressType.Picking
                && pdtFlr.Floor.Position.Row.Path.Store.Depository.Id == targetDep.Id);

            // if the product is in a picking address already, the new batch should go in a stock address
            if (existingStorage != null)
            {
                Position column = existingStorage.Floor.Position;
                List<Floor> occupiedFloors = _contextDb.ProductFloors.Include(pdtFlr => pdtFlr.Floor.Position.Row.Path.Store.Depository)
                    .Where(pdtFlr => pdtFlr.Floor.PositionId == column.Id && pdtFlr.Quantity > 0).Select(pdtFlr => pdtFlr.Floor).ToList();
                List<Floor> columnFloors = GetFloors(column).ToList();
                // if there is room for storage above the picking address use that
                if (occupiedFloors.Count < columnFloors.Count)
                {
                    List<Floor> vacantFloors = new List<Floor>();
                    columnFloors.ForEach(flr =>
                    {
                        if (!occupiedFloors.Exists(occupiedFloor => occupiedFloor.Id == flr.Id))
                            vacantFloors.Add(flr);
                    });
                    storageLocation = vacantFloors.FirstOrDefault(flr => flr.AddressType == AddressType.Stock);
                }
                // if there isn't use anything
                else
                {
                    storageLocation = _contextDb.Floors.AsNoTracking().Include(flr => flr.Position.Row.Path.Store.Depository).Include(flr => flr.Position.SupportType).FirstOrDefault(floor =>
                        floor.Position.Row.Path.Store.Depository.Id == targetDep.Id &&
                        floor.AddressType == AddressType.Stock &&
                        !_contextDb.ProductFloors.Any(pf => pf.FloorId == floor.Id && pf.Quantity > 0));//availableAddresses.FirstOrDefault(add => add.Floor.AddressType == AddressType.Stock).Floor;
                    if (storageLocation == default(Floor))
                    {
                        throw new ShowableException("Il n'y a plus d'adresse de stockage valide.");
                    }
                }
            }
            // If the product is not in a picking address, create one
            else
            {
                storageLocation = _contextDb.Floors.AsNoTracking().Include(flr => flr.Position.Row.Path.Store.Depository).Include(flr => flr.Position.SupportType).FirstOrDefault(floor =>
                    floor.Position.Row.Path.Store.Depository.Id == targetDep.Id &&
                    floor.AddressType == AddressType.Picking &&
                    !_contextDb.ProductFloors.Any(pf => pf.FloorId == floor.Id && pf.Quantity > 0));//availableAddresses.FirstOrDefault(add => add.Floor.AddressType == AddressType.Picking).Floor;
                if (storageLocation == default(Floor))
                {
                    throw new ShowableException("Il n'y a plus d'adresse de picking valide.");
                }
            }

            // Create a new association between the floor and the product and save it in the database
            ProductFloor association;
            if (traceAbilityInformation == null)
            {
                association = new ProductFloor
                {
                    Barcode = barcode = HelperGenerateBarcode(),
                    FloorId = storageLocation.Id,
                    ProductId = toStore.Id,
                    Quantity = quantity,
                    Stored = false,
                };
            }
            else
            {
                association = new ProductFloor
                {
                    Barcode = barcode = HelperGenerateBarcode(),
                    FloorId = storageLocation.Id,
                    ProductId = toStore.Id,
                    Quantity = quantity,
                    CustomsNumber = traceAbilityInformation.CustomsNumber,
                    LotNumber = traceAbilityInformation.LotNumber,
                    ExpirationDate = traceAbilityInformation.ExpirationDate,
                    ReceptionDate = traceAbilityInformation.ReceptionDate,
                    SerialNumber = traceAbilityInformation.SerialNumber,
                    Stored = false,
                };
            }
            _contextDb.ProductFloors.Add(association);
            _contextDb.SaveChanges();

            //return the location where the product should be saved to the user
            return new Address(storageLocation);
        }



        #region Helper


        public static string HelperAlphabetLetter(int index)
        {
            return Enumerable.Range('A', 26).Select(x => (char)x).ToArray()[index].ToString();
        }

        /// <summary>
        /// Generates a unique barcode.
        /// </summary>
        /// <returns>The barcode which has been generated</returns>
        public string HelperGenerateBarcode()
        {
            Random random = new Random();
            bool unique;
            string barcode;
            do
            {
                barcode = random.Next(100000, 999999).ToString() + random.Next(1000000, 9999999).ToString();
                unique = _contextDb.Positions.All(position => position.Barcode != barcode) && _contextDb.ProductFloors.All(pf => pf.Barcode != barcode) && _contextDb.Orders.All(order => order.Barcode != barcode);
            } while (!unique);
            return barcode;
        }

        /// <summary>
        /// Creates a graphic barcode corresponding to a numeric barcode
        /// </summary>
        /// <param name="barcode">The barcode to use</param>
        /// <returns>An image which contains the barcode</returns>
        public Image HelperGetGraphicBarcode(string barcode)
        {
            Code128BarcodeDraw generatedBarcode = BarcodeDrawFactory.Code128WithChecksum;
            return generatedBarcode.Draw(barcode, 50);
        }

        public StringBuilder HelperGenerateReceptionLabelToPrint(string barcode)
        {
            ProductFloor productFloor = _contextDb.ProductFloors.AsNoTracking().Include(PointF => PointF.Product).SingleOrDefault(pf => pf.Barcode == barcode);
            StringBuilder sheet = new StringBuilder();
            sheet.AppendLine("Fiche produit");
            sheet.AppendLine();
            sheet.AppendLine("Code de l'article : " + productFloor.ProductId);
            sheet.AppendLine("Libellé : " + productFloor.Product.Wording);
            if (!string.IsNullOrWhiteSpace(productFloor.CustomsNumber))
            {
                sheet.AppendLine("Quantité : " + productFloor.Quantity);
                sheet.AppendLine("Date de réception : " + productFloor.ReceptionDate.Value.ToShortDateString());
                sheet.AppendLine("Date de Péremption : " + productFloor.ExpirationDate.Value.ToShortDateString());
                sheet.AppendLine("Numéro de série : " + productFloor.SerialNumber);
                sheet.AppendLine("Numéro de Douane : " + productFloor.CustomsNumber);
                sheet.AppendLine("Numéro de Lot : " + productFloor.LotNumber);
            }

            return sheet;
        }
        #endregion

        #region Validate
        /// <summary>
        /// Validates a depository.
        /// </summary>
        /// <param name="depository">The depository to check</param>
        private void ValidateDepository(Depository depository)
        {
            if (string.IsNullOrWhiteSpace(depository.Name))
                throw new ShowableException("Le nom d'un dépôt ne peut pas être vide.");
            if (string.IsNullOrWhiteSpace(depository.Address))
                throw new ShowableException("L'adresse d'un dépôt ne peut pas être vide.");
            if (depository.Postcode.Length != 5)
                throw new ShowableException("Le code postal d'un dépôt doit contenir 5 caractères.");
            if (depository.Postcode.ToCharArray().Any(c => !char.IsNumber(c)))
                throw new ShowableException("Le code postal d'un dépôt ne doit contenir que des chiffres.");
            if (string.IsNullOrWhiteSpace(depository.City))
                throw new ShowableException("La ville d'un dépôt ne peut être vide.");

        }

        /// <summary>
        /// Validates a store.
        /// </summary>
        /// <param name="store">The store to validate</param>
        private void ValidateStore(Store store)
        {
            if (GetStores(store.Depository).Where(str => str.Id == store.Id).ToList().Count > MaxLimitOfStoreInDepository)
                throw new ShowableException("Le nombre limite de magasin a été atteint pour ce dépôt, impossible d'en créer un nouveau.");
            if (store.Depository == null)
                throw new ShowableException("Un magasin doit être rattaché à un dépôts.");
            ValidateDepository(store.Depository);
            if (string.IsNullOrWhiteSpace(store.Name))
                throw new ShowableException("Le nom d'un magasin ne peut pas être vide.");
            if (store.Width < 0)
                throw new ShowableException("La largeur d'un magasin ne peut pas être négative.");
            if (store.Lenght < 0)
                throw new ShowableException("La longueur d'un magasin ne peut pas être négative.");
        }

        /// <summary>
        /// Validates a path.
        /// </summary>
        /// <param name="path">The path to validate</param>
        private void ValidatePath(Path path)
        {
            List<Path> paths = GetPaths(path.Store).Where(pth => pth.Id != path.Id).ToList();
            if (paths.Count >= MaxLimitOfPathInStore)
                throw new ShowableException("Le nombre limite d'allée a été atteint pour ce magasin, impossible d'en créer un nouveau.");
            if (path.Store == null)
                throw new ShowableException("Une allée doit être rattachée à un magasin.");
            ValidateStore(path.Store);
            if (path.Name.Any(c => !char.IsUpper(c)) || path.Name.Any(c => !char.IsLetter(c)))
                throw new ShowableException("Le nom d'une allée doit être une lettre en majuscule.");
            if (paths.Any(pth => pth.Name == path.Name))
                throw new ShowableException("Une allée avec le même nom existe déjà dans ce magasin.");
            if (path.PathWidth < 0)
                throw new ShowableException("La largeur de l'allée ne peut pas être négative.");
            if (path.Lenght < 0)
                throw new ShowableException("La longeur de l'allée ne peut pas être négative.");
            if (path.StorageWidth < 0)
                throw new ShowableException("La largeur de la zone de stockage de l'allée ne peut pas être négative.");
            if (path.Lenght > path.Store.Lenght)
                throw new ShowableException("La longeur de l'allée ne peut pas être supérieur à celle de son magasin.");
            if (path.StorageWidth * (path.Unilateral ? 1 : 2) + path.PathWidth > path.Store.Width)
                throw new ShowableException("La largeur de l'allée ne peut pas être supérieur à celle de son magasin.");
            if (paths.Count > 0 && paths.Sum(pth => pth.StorageWidth * (pth.Unilateral ? 1 : 2) + pth.PathWidth) + path.StorageWidth * (path.Unilateral ? 1 : 2) + path.PathWidth > path.Store.Width)
                throw new ShowableException("Il n'y a plus assez de place dans le magasin pour une allée aussi large.");

        }

        /// <summary>
        /// Validates a row.
        /// </summary>
        /// <param name="row">The row to validate</param>
        private void ValidateRow(Row row)
        {
            if (row.Path == null)
                throw new ShowableException("Un rang doit être rattaché à une allée.");
            ValidatePath(row.Path);
            if (row.Name < 0)
                throw new ShowableException("Le nom d'un rang doit être un nombre non négataif.");

            List<Row> rows = GetRows(row.Path).Where(r => r.Id != row.Id).ToList();
            List<Row> sameSideRows = rows.Where(r => r.PairSide == row.PairSide).ToList();
            if (rows.Any(r => r.Name == row.Name))
                throw new ShowableException("Un rang avec le même nom existe déjà dans cette allée.");
            if (row.Width < 0)
                throw new ShowableException("La largeur du rang ne peut pas être négative.");
            if (row.Width > row.Path.Lenght)
                throw new ShowableException("La largeur du rang ne peut pas être supéreur à la longueur de l'allée.");
            if (!row.Path.Unilateral && row.Name % 2 == Convert.ToInt32(row.PairSide))
                throw new ShowableException("Le rang n'est pas du bon côté de l'allée.");
            if (row.Path.Unilateral && row.PairSide)
                throw new ShowableException("L'allée est unilaterale, le rang ne peut pas être d'un autre côté.");
            if (sameSideRows.Count > 0 && sameSideRows.Sum(r => r.Width) + row.Width > row.Path.Lenght)
                throw new ShowableException("Il n'y a plus assez de place dans l'allée pour un rang aussi large.");
        }

        /// <summary>
        /// Validates a position.
        /// </summary>
        /// <param name="position">The position to validate</param>
        private void ValidatePosition(Position position)
        {
            List<Position> positions = GetPositions(position.Row).Where(pos => pos.Id != position.Id).ToList();
            if (positions.Count >= MaxLimitOfPositionInRow)
                throw new ShowableException("Le nombre limite de position a été atteint pour ce rang, impossible d'en créer une nouvelle.");
            if (position.Row == null)
                throw new ShowableException("Une position doit être rattachée à un rang.");
            ValidateRow(position.Row);
            //if (position.Name!= "A" || position.Name != "B" || position.Name != "C")
            //    throw new ShowableException("Le nom d'une position doit être une lettre majuscule comprise entre A et C.");
            if (position.Name.Any(c => !char.IsUpper(c)) || position.Name.Any(c => !char.IsLetter(c)))
                throw new ShowableException("Le nom d'une position doit être une lettre majuscule.");
            if (positions.Any(pos => pos.Name == position.Name))
                throw new ShowableException("Une position avec le même nom existe déjà.");
            if (position.Barcode.Length != 13 || position.Barcode.ToArray().Any(c => !char.IsNumber(c)))
                throw new ShowableException("Le code barre doit contenir 13 chiffres.");
            if (position.SupportType == null)
                throw new ShowableException("La position doit être liée à un type de support.");
            if (position.Row.Width < position.SupportType.Width)
                throw new ShowableException("La largeur du type de support est supérieur à celle du rang.");
            if (position.Row.Path.StorageWidth < position.SupportType.Length)
                throw new ShowableException("La profondeur du type de support est supérieur à celle du rang.");
            if (positions.Count > 0 && positions.Sum(pos => pos.SupportType.Width) + position.SupportType.Width > position.Row.Width)
                throw new ShowableException("Il n'y a plus de place dans le rang pour une position de cette largueur");
        }

        /// <summary>
        /// Validates a floor.
        /// </summary>
        /// <param name="floor">The floor to validate</param>
        private void ValidateFloor(Floor floor)
        {
            List<Floor> floors = GetFloors(floor.Position).Where(flr => flr.Id != floor.Id).ToList();
            if (floors.Count >= MaxLimitOfAddressInStore)
                throw new ShowableException("Le nombre limite d'addresses à été atteinte pour ce magasin, vous ne pouvez pas en créer de nouvelle.");
            if (floor.Position == null)
                throw new ShowableException("Un étage doit être rattaché à une position.");
            ValidatePosition(floor.Position);
            if (floor.Value < 0 || floor.Value > 4)
                throw new ShowableException("La valeur de la position doit être un chiffre compris entre 0 et 4");

            if (floors.Any(flr => flr.Value == floor.Value))
                throw new ShowableException("Un étage avec la même valeur existe déjà.");

        }

        /// <summary>
        /// Validates a supportType.
        /// </summary>
        /// <param name="supportType">The supportType to validate</param>
        public void ValidateSupportType(SupportType supportType)
        {
            if (string.IsNullOrEmpty(supportType.Name))
                throw new ShowableException("Un type de support doit avoir un nom");
            if (supportType.Width < 0)
                throw new ShowableException("La largeur du type de support ne peut pas être négative.");
            if (supportType.Length < 0)
                throw new ShowableException("La longueur du type de support ne peut pas être négative.");
            if (supportType.Height < 0)
                throw new ShowableException("La height du type de support ne peut pas être négative.");
        }

        /// <summary>
        /// Validates a product.
        /// </summary>
        /// <param name="product">The product to validate</param>
        public void ValidateProduct(Product product)
        {
            if (string.IsNullOrEmpty(product.Wording))
                throw new ShowableException("Une produit doit avoir un libellé.");
            if (product.Wording.Length > 150)
                throw new ShowableException("Le libellé du produit ne doit pas faire plus de 150 caractères.");
            Product prod = _contextDb.Products.SingleOrDefault(x => x.Wording == product.Wording && x.Id != product.Id);
            if (prod != null && prod.IsActive)
                throw new ShowableException("Un produit ou un colis avec le même libellé existe déjà.");
            if (prod != null && !prod.IsActive)
                throw new ShowableException("Un produit ou un colis avec le même libellé existe déjà mais il est désactiver. Réactivez le pour pouvoir l'utiliser.");
        }

        /// <summary>
        /// Validates an internal movement.
        /// </summary>
        /// <param name="internalMovement">The internal movement to validate</param>
        public void ValidateInternalMovement(InternalMovement internalMovement)
        {
            if (internalMovement.StartingFloor == null)
                throw new ShowableException("Un mouvement interne doit avoir une adresse de départ.");
            if (internalMovement.FinalFloor == null)
                throw new ShowableException("Un mouvement interne doit avoir une adresse de fin.");
            if (internalMovement.MovedProduct == null)
                throw new ShowableException("Un mouvement interne doit concerner un produit.");
            if (internalMovement.Quantity <= 0)
                throw new ShowableException("Un mouvement interne doit concerner une quantité de produit supérieure à 0");
        }

        /// <summary>
        /// Validates a packet.
        /// </summary>
        /// <param name="packet">The packet to validate</param>
        private void ValidatePacket(Packet packet)
        {
            if (string.IsNullOrWhiteSpace(packet.Wording))
                throw new ShowableException("Le libellé d'un colis ne peut pas être vide.");
            if (packet.Wording.Length > 20)
                throw new ShowableException("Le libellé d'un colis ne peut pas dépasser 20 caractères.");

            Product prod = _contextDb.Products.SingleOrDefault(x => x.Wording == packet.Wording && x.Id != packet.Id);
            if (prod != null && prod.IsActive)
                throw new ShowableException("Un produit ou un colis avec le même libellé existe déjà.");
            if (prod != null && !prod.IsActive)
                throw new ShowableException("Un produit ou un colis avec le même libellé existe déjà mais il est désactiver. Réactivez le pour pouvoir l'utiliser.");
        }

        /// <summary>
        /// Validates an association between a product and a floor.
        /// </summary>
        /// <param name="productFloor">The association to validate</param>
        private void ValidateProductFloor(ProductFloor productFloor)
        {
            if (productFloor.ExpirationDate < productFloor.ReceptionDate)
                throw new ShowableException("La date de réception ne peut pas être inférieure à la date de répemption");
            if (productFloor.ReceptionDate > DateTime.Now)
                throw new ShowableException("La date de réception ne peut pas être supérieure à la date du jour");
        }
        #endregion

        #region Get

        /// <summary>
        /// Returns the list of all the products of an order
        /// </summary>
        /// <param name="numOrder"></param>
        /// <returns></returns>
        public List<Product> GetProductsOfOrder(string numOrder)
        {
            List<Product> products = GetOrderProducts().Where(orderProduct => orderProduct.Order.Barcode.Equals(numOrder)).Select(orderProduct => orderProduct.Product).ToList();

            return products;
        }

        /// <summary>
        /// Returns the list of all the products of an order
        /// </summary>
        /// <param name="numOrder"></param>
        /// <returns></returns>
        public List<Store> GetStoresOfOrder(int numOrder)
        {
            List<OrderProduct> orderProducts = GetOrderProducts().Where(x => x.OrderId.Equals(numOrder)).ToList();

            List<Store> theOrder = new List<Store>();

            foreach (var o in orderProducts)
            {
                foreach (var s in GetStores())
                {
                    //if (o.OrderId == s.Id)
                    //{
                    theOrder.Add(s);
                    //}
                }
            }

            return theOrder;
        }

        /// <summary>
        /// Return the list of all the products
        /// </summary>
        /// <returns></returns>
        public List<Product> GetProducts(bool onlyActive = true, bool withProductFloors = false)
        {
            List<Product> products = _contextDb.Products.AsNoTracking().ToList();
            foreach (Product prod in products)
            {
                if (prod.GetType() == typeof(Packet))
                    ((Packet)prod).Articles.AddRange(_contextDb.PacketArticles.Where(pa => pa.PacketId == prod.Id).ToList());
            }
            if (onlyActive)
                products = products.Where(x => x.IsActive).ToList();
            if (withProductFloors)
            {
                foreach (Product product in products)
                {
                    product.ProductFloors = _contextDb.ProductFloors.Include(x => x.Floor.Position.Row.Path.Store.Depository).Include(prdtFl => prdtFl.Floor.Position.SupportType).Include(pf => pf.Product).Where(x => x.Product.Id == product.Id).ToList();
                }
            }
            return products;
        }

        public List<Product> GetProductsToAddInPacket(Packet packet)
        {
            return _contextDb.Products.Where(product =>
                !_contextDb.PacketArticles.Any(pa => pa.Packet.Id == packet.Id && pa.Article.Id == product.Id) && product.Id != packet.Id).ToList();
        }

        /// <summary>
        /// Gets a product by using its id.
        /// </summary>
        /// <param name="id">The id of the product to search for</param>
        /// <returns>The product which has the specified id, or null if there is none</returns>
        public Product GetProductById(int id)
        {
            try { return _contextDb.Products.FirstOrDefault(pdt => pdt.Id == id); }
            catch (Exception)
            {
                Console.Write("Ce produit n'a jamais été crée");
                return null;
            };
        }

        /// <summary>
        /// Return the list of all the order products
        /// </summary>
        /// <returns></returns>
        public List<OrderProduct> GetOrderProducts()
        {
            return _contextDb.OrderProducts.AsNoTracking().Include(x => x.Product).Include(x => x.Order).ToList();
        }

        public List<Order> GetOrderProducts(Product product)
        {
            List<Order> orders = new List<Order>();
            if (product != null)
            {
                orders = GetOrderProducts().Where(x => x.ProductId == product.Id).Select(x => x.Order).Distinct().ToList();
            }
            return orders;
        }

        /// <summary>
        /// Returns a list of address contains by the depository passed in parameter
        /// </summary>
        /// <param name="depository">The depository which you want the list of address</param>
        /// <param name="onlyNotEmpty">true if you want to get only not empty addresses, only the addresses which have a product</param>
        /// <returns></returns>
        public List<Address> GetAddresses(Depository depository, bool onlyNotEmpty = false)
        {
            List<Address> adresses = new List<Address>();
            if (depository != null)
            {
                List<Store> stores = _contextDb.Stores.Where(store => store.DepositoryId == depository.Id).ToList();
                foreach (Store store in stores)
                {
                    List<Path> paths = _contextDb.Paths.Where(path => path.StoreId == store.Id).ToList();
                    foreach (Path path in paths)
                    {
                        List<Row> rows = _contextDb.Rows.Where(row => row.PathId == path.Id).ToList();
                        foreach (Row row in rows)
                        {
                            List<Position> positions = _contextDb.Positions.Where(pos => pos.RowId == row.Id).ToList();
                            foreach (Position position in positions)
                            {
                                List<Floor> floors = _contextDb.Floors.Where(flr => flr.PositionId == position.Id).ToList();
                                foreach (Floor floor in floors)
                                {
                                    if (onlyNotEmpty)
                                    {
                                        if (_contextDb.ProductFloors.Where(pdtFlr => pdtFlr.FloorId == floor.Id).Any(pdtFlr => pdtFlr.Quantity > 0))
                                            adresses.Add(new Address(floor));
                                    }
                                    else
                                        adresses.Add(new Address(floor));
                                }
                            }
                        }
                    }
                }
            }
            return adresses;
        }

        /// <summary>
        /// Returns a list of address contains by the store passed in parameter
        /// </summary>
        /// <param name="store">The store which you want the list of address</param>
        /// <param name="onlyNotEmpty">true if you want to get only not empty addresses, only the addresses which have a product</param>
        /// <returns></returns>
        public List<Address> GetAddresses(Store store, bool onlyNotEmpty = false)
        {
            List<Address> adresses = new List<Address>();
            if (store != null)
            {
                List<Path> paths = _contextDb.Paths.Where(x => x.StoreId == store.Id).ToList();
                foreach (Path path in paths)
                {
                    List<Row> rows = _contextDb.Rows.Where(x => x.PathId == path.Id).ToList();
                    foreach (Row row in rows)
                    {
                        List<Position> positions = _contextDb.Positions.Where(x => x.RowId == row.Id).ToList();
                        foreach (Position position in positions)
                        {
                            List<Floor> floors = _contextDb.Floors.Where(x => x.PositionId == position.Id).ToList();
                            foreach (Floor floor in floors)
                            {
                                if (onlyNotEmpty)
                                {
                                    if (_contextDb.ProductFloors.Where(x => x.FloorId == floor.Id).Any(x => x.Quantity == 0))
                                        adresses.Add(new Address(floor));
                                }
                                else
                                    adresses.Add(new Address(floor));
                            }
                        }
                    }
                }
            }
            return adresses;
        }

        /// <summary>
        /// Returns a list of address contains by the path passed in parameter
        /// </summary>
        /// <param name="path">The path which you want the list of address</param>
        /// <param name="onlyNotEmpty">true if you want to get only not empty addresses, only the addresses which have a product</param>
        /// <returns></returns>
        public List<Address> GetAddresses(Path path, bool onlyNotEmpty = false)
        {
            List<Address> adresses = new List<Address>();
            if (path != null)
            {
                List<Row> rows = _contextDb.Rows.Where(x => x.PathId == path.Id).ToList();
                foreach (Row row in rows)
                {
                    List<Position> positions = _contextDb.Positions.Where(x => x.RowId == row.Id).ToList();
                    foreach (Position position in positions)
                    {
                        List<Floor> floors = _contextDb.Floors.Where(x => x.PositionId == position.Id).ToList();
                        foreach (Floor floor in floors)
                        {
                            if (onlyNotEmpty)
                            {
                                if (_contextDb.ProductFloors.Where(x => x.FloorId == floor.Id).Any(x => x.Quantity == 0))
                                    adresses.Add(new Address(floor));
                            }
                            else
                                adresses.Add(new Address(floor));
                        }
                    }
                }

            }
            return adresses;
        }

        /// <summary>
        /// Returns a list of address contains by the row passed in parameter
        /// </summary>
        /// <param name="row">The row which you want the list of address</param>
        /// <param name="onlyNotEmpty">true if you want to get only not empty addresses, only the addresses which have a product</param>
        /// <returns></returns>
        public List<Address> GetAddresses(Row row, bool onlyNotEmpty = false)
        {
            List<Address> adresses = new List<Address>();
            if (row != null)
            {
                List<Position> positions = _contextDb.Positions.Where(x => x.RowId == row.Id).ToList();
                foreach (Position position in positions)
                {
                    List<Floor> floors = _contextDb.Floors.Where(x => x.PositionId == position.Id).ToList();
                    foreach (Floor floor in floors)
                    {
                        if (onlyNotEmpty)
                        {
                            if (_contextDb.ProductFloors.Where(x => x.FloorId == floor.Id).Any(x => x.Quantity == 0))
                                adresses.Add(new Address(floor));
                        }
                        else
                            adresses.Add(new Address(floor));
                    }
                }
            }
            return adresses;
        }

        /// <summary>
        /// Returns a list of address contains by the position passed in parameter
        /// </summary>
        /// <param name="position">The postion which you want the list of address</param>
        /// <param name="onlyNotEmpty">true if you want to get only not empty addresses, only the addresses which have a product</param>
        /// <returns></returns>
        public List<Address> GetAddresses(Position position, bool onlyNotEmpty = false)
        {
            List<Address> adresses = new List<Address>();
            if (position != null)
            {
                List<Floor> floors = _contextDb.Floors.Where(x => x.PositionId == position.Id).ToList();
                foreach (Floor floor in floors)
                {
                    if (onlyNotEmpty)
                    {
                        if (_contextDb.ProductFloors.Where(x => x.FloorId == floor.Id).Any(x => x.Quantity == 0))
                            adresses.Add(new Address(floor));
                    }
                    else
                        adresses.Add(new Address(floor));
                }
            }
            return adresses;
        }

        /// <summary>
        /// Return the adress of an article
        /// </summary>
        /// <returns></returns>
        public List<Address> GetAddresses(int codeProduct, bool onlyNotEmpty = false)
        {
            List<Floor> floors;
            if (onlyNotEmpty)
                floors = GetProducts(true, true).SingleOrDefault(x => x.Id == codeProduct)?.ProductFloors?.Where(x => x.Quantity != 0)?.Select(x => x.Floor).ToList();
            else
                floors = GetProducts(true, true).SingleOrDefault(x => x.Id == codeProduct)?.ProductFloors?.Select(x => x.Floor).ToList();
            List<Address> addresses = new List<Address>();
            if (floors != null)
            {
                foreach (var floor in floors)
                {
                    if (floor != null)
                    {
                        addresses.Add(new Address(floor));
                    }
                }
            }

            return addresses;
        }

        /// <summary>
        /// Gets an association between a product and a floor.
        /// </summary>
        /// <param name="product">The product to search for</param>
        /// <param name="quantity">The minimum quantity to have in store</param>
        /// <returns>An association with the specified prodcut and any floor, or null if there is none</returns>
        public ProductFloor GetProductFloor(Product product, int quantity, List<ProductFloor> ProductFloorsAlreadyUsed)
        {
            if (product != null)
            {
                List<ProductFloor> list = _contextDb.ProductFloors.Include(pf => pf.Floor.Position.Row.Path.Store.Depository)
                    .Include(pf => pf.Floor.Position.SupportType).Include(pf => pf.Product)
                    .Where(pf => pf.ProductId == product.Id && !ProductFloorsAlreadyUsed.Any(pfU => pfU.ProductId == pf.ProductId && pfU.FloorId == pf.FloorId)).ToList(); 
                list.OrderBy(x => x.ExpirationDate).ThenBy(x => x.Quantity);
                return list.FirstOrDefault();
            }
            else
                return null;
        }

        /// <summary>
        /// Returns all the movements they can be possible.
        /// </summary>
        /// <returns></returns>
        public List<InternalMovement> GetMovements(List<String> arguments, Boolean combobox)
        {
            List<InternalMovement> listeMvmts = new List<InternalMovement>();

            string numArticle = arguments[0];
            string numDouane = arguments[1];
            string numClient = arguments[2];
            string numReception = arguments[3];
            string depositorie = arguments[4];
            string store = arguments[5];
            string path = arguments[6];
            string row = arguments[7];
            string position = arguments[8];
            string floor = arguments[9];

            foreach (Product p in GetProducts(true, true))
            {
                List<ProductFloor> pickingAddress = new List<ProductFloor>();
                List<ProductFloor> stockAddress = new List<ProductFloor>();
                if (p.ProductFloors != null)
                {
                    if (combobox)
                    {
                        foreach (ProductFloor pf in p.ProductFloors.Where(x => (numArticle != "" ? x.Barcode.ToString() == numArticle : true)
                                                                      && (numDouane != "" ? Convert.ToString(x.CustomsNumber) == numDouane : true)
                                                                      && (floor != "" ? Convert.ToString(x.Floor.Id) == floor : true)
                                                                      && (position != "" ? Convert.ToString(x.Floor.Position.Name) == position : true)
                                                                      && (row != "" ? Convert.ToString(x.Floor.Position.Row.Name) == row : true)
                                                                      && (path != "" ? Convert.ToString(x.Floor.Position.Row.Path.Name) == path : true)
                                                                      && (store != "" ? Convert.ToString(x.Floor.Position.Row.Path.Store.Name) == store : true)
                                                                      && (depositorie != "" ? Convert.ToString(x.Floor.Position.Row.Path.Store.Depository.Name) == depositorie : true)).ToList())
                        {
                            if (pf.Floor.AddressType == AddressType.Picking)
                                pickingAddress.Add(pf);
                            else if (pf.Floor.AddressType == AddressType.Stock)
                            {
                                stockAddress.Add(pf);
                            }
                        }
                    }
                    else
                    {
                        foreach (ProductFloor pf in p.ProductFloors)
                        {
                            if (pf.Floor.AddressType == AddressType.Picking)
                                pickingAddress.Add(pf);
                            else if (pf.Floor.AddressType == AddressType.Stock)
                            {
                                stockAddress.Add(pf);
                            }
                        }
                    }
                    // Sorting Stock Address list by Quantity and Expiration Date.
                    // new version of order by
                    stockAddress.OrderBy(x => x.ExpirationDate).ThenBy(x => x.Quantity);

                    // For the products they don't have picking address but they have stock address.
                    if (pickingAddress.Count == 0 && stockAddress.Count > 0)
                    {
                        // Sets the final location of the product.
                        // It must be a picking address, the floor chosen must be free and the support type must be the same as the storage address
                        Floor finalFloor = GetFloors().FirstOrDefault(flr => flr.AddressType == AddressType.Picking && !_contextDb.ProductFloors.Any(pdtflr => pdtflr.Floor.Id == flr.Id) && flr.Position.SupportType.Id == stockAddress.First().Floor.Position.SupportType.Id);
                        while (finalFloor == null && stockAddress.Count > 0)
                        {
                            finalFloor = GetFloors().FirstOrDefault(flr => flr.AddressType == AddressType.Picking && !_contextDb.ProductFloors.Any(pdtflr => pdtflr.Floor.Id == flr.Id) && flr.Position.SupportType.Id == stockAddress.First().Floor.Position.SupportType.Id);
                            stockAddress.Remove(stockAddress.First());
                        }
                        // Create internal movement based upon the new location of the product.
                        if (finalFloor != null)
                            listeMvmts.Add(new InternalMovement { ProductFloorConcerned = stockAddress.First(), StartingFloor = stockAddress.First().Floor, FinalFloor = finalFloor, MovedProduct = stockAddress.First().Product, Quantity = stockAddress.First().Quantity });
                    }
                }
            }
            if (combobox)
                return listeMvmts;
            else
                return listeMvmts.Where(x => (numArticle != "" ? x.ProductFloorConcerned.Barcode.ToString() == numArticle : true)
                                                                      && (numDouane != "" ? Convert.ToString(x.ProductFloorConcerned.CustomsNumber) == numDouane : true)
                                                                      && (floor != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Id) == floor : true)
                                                                      && (position != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Position.Name) == position : true)
                                                                      && (row != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Position.Row.Name) == row : true)
                                                                      && (path != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Position.Row.Path.Name) == path : true)
                                                                      && (store != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Position.Row.Path.Store.Name) == store : true)
                                                                      && (depositorie != "" ? Convert.ToString(x.ProductFloorConcerned.Floor.Position.Row.Path.Store.Depository.Name) == depositorie : true)).ToList();
        }

        /// <summary>
        /// Adjust the list of movements with the new movement selected.
        /// </summary>
        /// <param name="movSelected"></param>
        /// <param name="allMovs"></param>
        /// <returns></returns>
        public List<InternalMovement> GetMovementsAdjusted(InternalMovement movSelected, List<InternalMovement> allMovs)
        {
            List<InternalMovement> movDeleted = new List<InternalMovement>();
            foreach (InternalMovement intMov in allMovs)
            {
                // Si il s'agit du même produit à déplacer OU s'il il s'agissait du même emplacement de destination.
                if (intMov.StartingFloor == movSelected.StartingFloor || intMov.FinalFloor == movSelected.FinalFloor)
                {
                    movDeleted.Add(intMov);
                }
            }
            foreach (InternalMovement intMov in movDeleted)
            {
                allMovs.Remove(intMov);
            }
            movDeleted.Clear();
            return allMovs;
        }

        /// <summary>
        /// Returns all the types of support.
        /// </summary>
        /// <returns></returns>
        public List<SupportType> GetSupportTypes()
        {
            return _contextDb.SupportTypes.AsNoTracking().ToList();
        }

        /// <summary>
        /// Return the list of all the depositories
        /// </summary>
        /// <returns></returns>
        public List<Depository> GetDepositories()
        {
            List<Depository> depositories = _contextDb.Depositories.AsNoTracking().ToList();
            return depositories;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Depository GetDepository(int id)
        {
            return _contextDb.Depositories.AsNoTracking().SingleOrDefault(dep => dep.Id == id);
        }

        /// <summary>
        /// Return a list of stores. If the depository is not null, returns the stores of the depositories
        /// </summary>
        /// <param name="depository">The depository for which we want the list of deposits </param>
        /// <returns></returns>
        public List<Store> GetStores(Depository depository = null)
        {
            if (depository == null)
                return _contextDb.Stores.AsNoTracking().Include(store => store.Depository).ToList();
            else
                return _contextDb.Stores.AsNoTracking().Where(store => store.Depository.Id.Equals(depository.Id)).Include(store => store.Depository).ToList();
        }

        /// <summary>
        /// Returns a store.
        /// </summary>
        /// <param name="id">The id of the store which you want</param>
        /// <returns></returns>
        public Store GetStore(int id)
        {
            return _contextDb.Stores.AsNoTracking().Include(store => store.Depository).SingleOrDefault(store => store.Id == id);
        }

        /// <summary>
        /// Returns the paths they belong to the store if the store is not null, else returns the list of all paths.
        /// </summary>
        /// <param name="store">The store which you want its list of paths</param>
        /// <returns></returns>
        public List<Path> GetPaths(Store store = null)
        {
            if (store == null)
                return _contextDb.Paths.AsNoTracking().Include(x => x.Store.Depository).ToList();
            else
                return _contextDb.Paths.AsNoTracking().Where(x => x.StoreId.Equals(store.Id)).Include(x => x.Store.Depository).ToList();
        }

        /// <summary>
        /// Returns the paths they belong to the depository.
        /// </summary>
        /// <param name="depository"></param>
        /// <returns></returns>
        public List<Path> GetPaths(Depository depository)
        {
            return _contextDb.Paths.AsNoTracking().Where(path => path.Store.DepositoryId == depository.Id).Include(x => x.Store.Depository).ToList();
        }

        /// <summary>
        /// Gets a path by its id.
        /// </summary>
        /// <param name="id">The id of the path to search for</param>
        /// <returns>The path which has the specified id, or null if there is none</returns>
        public Path GetPath(int id)
        {
            Path test = _contextDb.Paths.AsNoTracking().Include(path => path.Store.Depository).SingleOrDefault(path => path.Id == id);
            return test;
        }

        /// <summary>
        /// Returns the rows they belong to the path if the path is not null, else returns the list of all rows.
        /// </summary>
        /// <param name="path">The path which you want its list of rows</param>
        /// <returns></returns>
        public List<Row> GetRows(Path path = null)
        {
            if (path == null)
                return _contextDb.Rows.AsNoTracking().Include(row => row.Path.Store.Depository).ToList();
            else
                return _contextDb.Rows.AsNoTracking().Where(row => row.PathId.Equals(path.Id)).Include(row => row.Path.Store.Depository).ToList();
        }

        /// <summary>
        /// Returns the rows they belong to the store if the store is not null, else returns the list of all rows.
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public List<Row> GetRows(Store store)
        {
            return _contextDb.Rows.AsNoTracking().Where(row => row.Path.StoreId == store.Id).Include(row => row.Path.Store.Depository).ToList();
        }

        public Row GetRow(int id)
        {
            return _contextDb.Rows.AsNoTracking().Include(row => row.Path.Store.Depository).SingleOrDefault(row => row.Id == id);
        }

        /// <summary>
        /// Returns the rows they belong to the depository.
        /// </summary>
        /// <param name="depository">The depository which you want its list of rows</param>
        /// <returns></returns>
        public List<Row> GetRows(Depository depository)
        {
            return _contextDb.Rows.AsNoTracking().Where(row => row.Path.Store.DepositoryId == depository.Id).Include(row => row.Path.Store.Depository).ToList();
        }

        /// <summary>
        /// Returns the positions they belong to the row if the row is not null, else returns the list of all positions.
        /// </summary>
        /// <param name="row">The row which you want its list of positions</param>
        /// <returns></returns>
        public List<Position> GetPositions(Row row = null)
        {
            if (row == null)
                return _contextDb.Positions.AsNoTracking().Include(position => position.Row.Path.Store.Depository).Include(x => x.SupportType).ToList();
            else
                return _contextDb.Positions.AsNoTracking().Where(position => position.Row.Id.Equals(row.Id)).Include(position => position.Row.Path.Store.Depository).Include(position => position.SupportType).ToList();
        }

        /// <summary>
        /// Returns the positions they belong to the depository.
        /// </summary>
        /// <param name="depository">The depository which you want its list of positions</param>
        /// <returns></returns>
        public List<Position> GetPositions(Depository depository)
        {
            return _contextDb.Positions.AsNoTracking().Where(position => position.Row.Path.Store.DepositoryId == depository.Id).Include(x => x.Row.Path.Store.Depository).Include(x => x.SupportType).ToList();
        }

        /// <summary>
        /// Returns the positions they belong to the store.
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public List<Position> GetPositions(Store store)
        {
            return _contextDb.Positions.AsNoTracking().Where(position => position.Row.Path.StoreId == store.Id).Include(x => x.Row.Path.Store.Depository).Include(x => x.SupportType).ToList();
        }

        /// <summary>
        /// Returns the positions they belong to the path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<Position> GetPositions(Path path)
        {
            return _contextDb.Positions.AsNoTracking().Where(position => position.Row.PathId == path.Id).Include(x => x.Row.Path.Store.Depository).Include(x => x.SupportType).ToList();
        }

        public Position GetPosition(int id)
        {
            return _contextDb.Positions.AsNoTracking().Include(position => position.Row.Path.Store.Depository).Include(position => position.SupportType).SingleOrDefault(position => position.Id == id);
        }

        /// <summary>
        /// Returns the floors they belong to the position if the position is not null, else returns the list of all floors.
        /// </summary>
        /// <param name="position">The position which you want its list of floors</param>
        /// <returns></returns>
        public List<Floor> GetFloors(Position position = null)
        {
            if (position == null)
                return _contextDb.Floors.AsNoTracking().Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
            else
                return _contextDb.Floors.AsNoTracking().Where(floor => floor.Position.Id.Equals(position.Id)).Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
        }

        /// <summary>
        /// Returns the floors they belong to the depository.
        /// </summary>
        /// <param name="depository">The depository which you want its list of floors</param>
        /// <returns></returns>
        public List<Floor> GetFloors(Depository depository)
        {
            return _contextDb.Floors.AsNoTracking().Where(floor => floor.Position.Row.Path.Store.Depository.Id == depository.Id).Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
        }

        /// <summary>
        /// Returns the floors they belong to the store.
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        public List<Floor> GetFloors(Store store)
        {
            return _contextDb.Floors.AsNoTracking().Where(floor => floor.Position.Row.Path.StoreId == store.Id).Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
        }

        /// <summary>
        /// Returns the floors they belong to the path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public List<Floor> GetFloors(Path path)
        {
            return _contextDb.Floors.AsNoTracking().Where(floor => floor.Position.Row.PathId == path.Id).Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
        }

        /// <summary>
        /// Returns the floors they belong to the row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public List<Floor> GetFloors(Row row)
        {
            return _contextDb.Floors.AsNoTracking().Where(floor => floor.Position.RowId == row.Id).Include(floor => floor.Position.Row.Path.Store.Depository).Include(floor => floor.Position.SupportType).ToList();
        }

        /// <summary>
        /// Returns all registered Products that are not yet stored
        /// </summary>
        /// <returns></returns>
        public List<ProductFloor> GetRegisteredProducts()
        {
            string[] tabLetters = {"A", "B", "C", "D", "E", "F", "G", "H", "I",
                "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            // Retrieve the list of registered products that are not yet stored
            List<ProductFloor> products = _contextDb.ProductFloors.AsNoTracking().Where(x => x.Stored == false).Include(x => x.Floor.Position.Row.Path.Store.Depository).Include(x => x.Product).Include(x => x.Floor.Position.SupportType).ToList();
            // Sort products
            products.OrderBy(x => x.Floor.Position.Row.Path.Store.Depository.Name)
                .ThenBy(x => x.Floor.Position.Row.Path.Store.Name)
                .ThenBy(x => x.Floor.Position.Row.Path.Name)
                .ThenBy(x => x.Floor.Position.Name)
                .ThenBy(x => x.Floor.Id);
            return products;
        }

        public CustomerOrder GetNextOrder()
        {
            return _contextDb.Orders.AsNoTracking().OfType<CustomerOrder>().OrderBy(Order => Order.OrderDate).FirstOrDefault(ord => ord.Prepared == OrderState.Validated);
        }


        #endregion

        #region Create

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository)
        {

            return CreateDepository(depository, -1, -1, -1, -1, -1, -1, -1, false, -1, -1, -1, null, -1, -1, DefaultAddressType);

        }

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <param name="numberOfStores">The desired number of stores</param>
        /// <param name="storeLength">The desired length of each store</param>
        /// <param name="storeWidth">The desired width of each store</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository, int numberOfStores, int storeLength, int storeWidth)
        {

            return CreateDepository(depository, numberOfStores, storeLength, storeWidth, -1, -1, -1, -1, false, -1, -1, -1, null, -1, -1, DefaultAddressType);

        }

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <param name="numberOfStores">The desired number of stores</param>
        /// <param name="storeLength">The desired length of each store</param>
        /// <param name="storeWidth">The desired width of each store</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository, int numberOfStores, int storeLength, int storeWidth, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral)
        {

            return CreateDepository(depository, numberOfStores, storeLength, storeWidth, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, -1, -1, -1, null, -1, -1, DefaultAddressType);

        }

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <param name="numberOfStores">The desired number of stores</param>
        /// <param name="storeLength">The desired length of each store</param>
        /// <param name="storeWidth">The desired width of each store</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository, int numberOfStores, int storeLength, int storeWidth, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth)
        {

            return CreateDepository(depository, numberOfStores, storeLength, storeWidth, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, numberOfRows, defaultRowWidth, -1, null, -1, -1, DefaultAddressType);

        }

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <param name="numberOfStores">The desired number of stores</param>
        /// <param name="storeLength">The desired length of each store</param>
        /// <param name="storeWidth">The desired width of each store</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository, int numberOfStores, int storeLength, int storeWidth, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType)
        {

            return CreateDepository(depository, numberOfStores, storeLength, storeWidth, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, numberOfRows, defaultRowWidth, numberOfPositions, supportType, -1, -1, DefaultAddressType);

        }

        /// <summary>
        /// Creates a depository.
        /// </summary>
        /// <param name="depository">The depository to create</param>
        /// <param name="numberOfStores">The desired number of stores</param>
        /// <param name="storeLength">The desired length of each store</param>
        /// <param name="storeWidth">The desired width of each store</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <param name="numberOfFloors">The desired number of floors for each position</param>
        /// <param name="heightLimit">The height limit for each position</param>
        /// <param name="addressType">The default address type</param>
        /// <returns>The id of the created depository</returns>
        public int CreateDepository(Depository depository, int numberOfStores, int storeLength, int storeWidth, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType, int numberOfFloors, int heightLimit, AddressType addressType)
        {
            ValidateDepository(depository);
            Depository depCreated = _contextDb.Depositories.Add(depository);
            for (int storeIt = 0; storeIt < numberOfStores; ++storeIt)
            {
                Store storeToCreate = new Store()
                {
                    Depository = depCreated,
                    Lenght = storeLength,
                    Name = storeIt.ToString(),
                    Width = storeWidth
                };
                CreateStore(storeToCreate, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, numberOfRows, defaultRowWidth, numberOfPositions, supportType, numberOfFloors, heightLimit, addressType);
            }
            _contextDb.SaveChanges();
            return depCreated.Id;
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <param name="store">The store to create</param>
        /// <returns>The id of the created store</returns>
        public int CreateStore(Store store)
        {
            return CreateStore(store, -1, -1, -1, -1, false, -1, -1, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <param name="store">The store to create</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <returns>The id of the created store</returns>
        public int CreateStore(Store store, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral)
        {
            return CreateStore(store, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, -1, -1, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <param name="store">The store to create</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <returns>The id of the created store</returns>
        public int CreateStore(Store store, int numberOfPaths, int defaultPathLength, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth)
        {
            return CreateStore(store, numberOfPaths, defaultPathLength, defaultPathWidth, defaultPathStorageWidth, unilateral, numberOfRows, defaultRowWidth, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <param name="store">The store to create</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <returns>The id of the created store</returns>
        public int CreateStore(Store store, int numberOfPaths, int defaultPathLenght, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType)
        {
            return CreateStore(store, numberOfPaths, defaultPathLenght, defaultPathWidth, defaultPathStorageWidth, unilateral, numberOfRows, defaultRowWidth, numberOfPositions, supportType, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a store.
        /// </summary>
        /// <param name="store">The store to create</param>
        /// <param name="numberOfPaths">The desired number of paths</param>
        /// <param name="defaultPathLength">The desired length of each path</param>
        /// <param name="defaultPathWidth">The desired width of each path</param>
        /// <param name="defaultPathStorageWidth">The desired width of the storage</param>
        /// <param name="unilateral">Whether paths are to be unilateral or not</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <param name="numberOfFloors">The desired number of floors for each position</param>
        /// <param name="heightLimit">The height limit for each position</param>
        /// <param name="addressType">The default address type</param>
        /// <returns>The id of the created store</returns>
        public int CreateStore(Store store, int numberOfPaths, int defaultPathLenght, int defaultPathWidth, int defaultPathStorageWidth, bool unilateral, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType, int numberOfFloors, int heightLimit, AddressType addressType)
        {
            ValidateStore(store);
            Store createdStore = _contextDb.Stores.Add(store);
            for (int positionIt = 0; positionIt < numberOfPaths; ++positionIt)
            {
                Path pathToCreate = new Path()
                {
                    Lenght = defaultPathLenght,
                    Name = HelperAlphabetLetter(positionIt),
                    PathWidth = defaultPathWidth,
                    StorageWidth = defaultPathStorageWidth,
                    Store = createdStore,
                    Unilateral = unilateral
                };
                CreatePath(pathToCreate, numberOfRows, defaultRowWidth, numberOfPositions, supportType, numberOfFloors, heightLimit, addressType);
            }
            _contextDb.SaveChanges();
            return createdStore.Id;
        }

        /// <summary>
        /// Creates a path.
        /// </summary>
        /// <param name="path">The path to create</param>
        /// <returns>The id of the path to be created</returns>
        public int CreatePath(Path path)
        {
            return CreatePath(path, -1, -1, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a path.
        /// </summary>
        /// <param name="path">The path to create</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <returns>The id of the path to be created</returns>
        public int CreatePath(Path path, int numberOfRows, int defaultRowWidth)
        {
            return CreatePath(path, numberOfRows, defaultRowWidth, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a path.
        /// </summary>
        /// <param name="path">The path to create</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <returns>The id of the path to be created</returns>
        public int CreatePath(Path path, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType)
        {
            return CreatePath(path, numberOfRows, defaultRowWidth, numberOfPositions, supportType, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a path.
        /// </summary>
        /// <param name="path">The path to create</param>
        /// <param name="numberOfRows">The desired number of rows</param>
        /// <param name="defaultRowWidth">The desired width of each row</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <param name="numberOfFloors">The desired number of floors for each position</param>
        /// <param name="heightLimit">The height limit for each position</param>
        /// <param name="addressType">The default address type</param>
        /// <returns>The id of the path to be created</returns>
        public int CreatePath(Path path, int numberOfRows, int defaultRowWidth, int numberOfPositions, SupportType supportType, int numberOfFloors, int heightLimit, AddressType addressType)
        {
            ValidatePath(path);
            Path createdPath = _contextDb.Paths.Add(path);
            for (int positionIt = 0; positionIt < numberOfRows; ++positionIt)
            {
                Row rowToCreate = new Row()
                {
                    Name = positionIt + 1,
                    Path = createdPath,
                    Width = defaultRowWidth,
                    PairSide = createdPath.Unilateral ? false : !Convert.ToBoolean((positionIt + 1) % 2)
                };
                CreateRow(rowToCreate, numberOfPositions, supportType, numberOfFloors, heightLimit, addressType);
            }
            _contextDb.SaveChanges();
            return createdPath.Id;
        }

        /// <summary>
        /// Creates a row.
        /// </summary>
        /// <param name="row">The row to create</param>
        /// <returns>The id of the created row</returns>
        public int CreateRow(Row row)
        {
            return CreateRow(row, -1, null, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a row.
        /// </summary>
        /// <param name="row">The row to create</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <returns>The id of the created row</returns>
        public int CreateRow(Row row, int numberOfPositions, SupportType supportType)
        {
            return CreateRow(row, numberOfPositions, supportType, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a row.
        /// </summary>
        /// <param name="row">The row to create</param>
        /// <param name="numberOfPositions">The desired number of positions</param>
        /// <param name="supportType">The support type to use by default</param>
        /// <param name="numberOfFloors">The desired number of floors for each position</param>
        /// <param name="heightLimit">The height limit for each position</param>
        /// <param name="addressType">The default address type</param>
        /// <returns>The id of the created row</returns>
        public int CreateRow(Row row, int numberOfPositions, SupportType supportType, int numberOfFloors, int heightLimit, AddressType addressType)
        {
            ValidateRow(row);
            Row createdRow = _contextDb.Rows.Add(row);
            for (int positionIt = 0; positionIt < numberOfPositions; ++positionIt)
            {
                Position positionToCreate = new Position()
                {
                    Barcode = HelperGenerateBarcode(),
                    Name = HelperAlphabetLetter(positionIt),
                    Row = createdRow,
                    SupportType = supportType ?? _contextDb.SupportTypes.First()
                };
                CreatePosition(positionToCreate, numberOfFloors, heightLimit, addressType);
            }
            _contextDb.SaveChanges();
            return createdRow.Id;
        }

        /// <summary>
        /// Creates a position.
        /// </summary>
        /// <param name="position">The position to create</param>
        /// <returns>The id of the created position</returns>
        public int CreatePosition(Position position)
        {
            return CreatePosition(position, -1, -1, DefaultAddressType);
        }

        /// <summary>
        /// Creates a position.
        /// </summary>
        /// <param name="position">The position to create</param>
        /// <param name="numberOfFloors">The desired number of floors for each position</param>
        /// <param name="heightLimit">The height limit for each position</param>
        /// <param name="addressType">The default address type</param>
        /// <returns>The id of the created position</returns>
        public int CreatePosition(Position position, int numberOfFloors, int heightLimit, AddressType addressType)
        {
            ValidatePosition(position);
            Position createdPosition = _contextDb.Positions.Add(position);
            for (int floorIt = 0; floorIt < numberOfFloors; ++floorIt)
            {
                Floor floorToCreate = new Floor()
                {
                    AddressType = floorIt > heightLimit ? AddressType.Stock : addressType,
                    Position = createdPosition,
                    Value = floorIt
                };
                CreateFloor(floorToCreate);
            }
            _contextDb.SaveChanges();
            return createdPosition.Id;
        }

        /// <summary>
        /// Creates a floor.
        /// </summary>
        /// <param name="floor">The floor to create</param>
        /// <returns>The id of the created floor</returns>
        public int CreateFloor(Floor floor)
        {
            ValidateFloor(floor);
            Floor fl = _contextDb.Floors.Add(floor);
            _contextDb.SaveChanges();
            return fl.Id;
        }

        /// <summary>
        /// Creates a support type.
        /// </summary>
        /// <param name="support">The support type to create</param>
        /// <returns>The id of the created support type</returns>
        public int CreateSupportType(SupportType support)
        {
            ValidateSupportType(support);
            SupportType st = _contextDb.SupportTypes.Add(support);
            _contextDb.SaveChanges();
            return st.Id;
        }

        /// <summary>
        /// Creates a product.
        /// </summary>
        /// <param name="product">The product to create</param>
        /// <returns>The id of the created product</returns>
        public int CreateProduct(Product product)
        {
            ValidateProduct(product);
            Product prod = _contextDb.Products.Add(product);
            _contextDb.SaveChanges();
            return prod.Id;
        }

        /// <summary>
        /// Creates a packet.
        /// </summary>
        /// <param name="packet">The packet to create</param>
        /// <param name="products">The products in the packet to create</param>
        /// <returns>The id of the created packet</returns>
        public int CreatePacket(Packet packet, List<Tuple<Product, int>> products)
        {
            if (products == null || products.Count < 1)
                throw new ShowableException("Un colis doit contenir au moins un produit.");
            ValidatePacket(packet);

            foreach (Tuple<Product, int> product in products)
            {
                PacketArticle toCreate = new PacketArticle() { ArticlesId = product.Item1.Id, PacketId = packet.Id, Quantity = product.Item2 };
                _contextDb.PacketArticles.Add(toCreate);
            }
            Packet packetToCreate = (Packet)_contextDb.Products.Add(packet);
            _contextDb.SaveChanges();
            return packetToCreate.Id;
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates the specified Depository
        /// </summary>
        /// <param name="depository">The depository which we want to update</param>
        public void UpdateDepository(Depository depository)
        {
            ValidateDepository(depository);
            Depository tmpDep = _contextDb.Depositories.SingleOrDefault(x => x.Id == depository.Id);
            tmpDep.Name = depository.Name;
            tmpDep.Address = depository.Address;
            tmpDep.Postcode = depository.Postcode;
            tmpDep.City = depository.City;
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Updates the specified store.
        /// </summary>
        /// <param name="store">The store which we want to update</param>
        public void UpdateStore(Store store)
        {
            ValidateStore(store);
            Store tmpStore = _contextDb.Stores.SingleOrDefault(x => x.Id == store.Id);
            tmpStore.Name = store.Name;
            tmpStore.Depository = store.Depository;
            tmpStore.DepositoryId = store.DepositoryId;
            tmpStore.Lenght = store.Lenght;
            tmpStore.Width = store.Width;
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Updates the specified path.
        /// </summary>
        /// <param name="path">The path which we want to update</param>
        public void UpdatePath(Path path)
        {
            ValidatePath(path);
            Path tmpPath = _contextDb.Paths.SingleOrDefault(x => x.Id == path.Id);
            tmpPath.Name = path.Name;
            tmpPath.Lenght = path.Lenght;
            tmpPath.PathWidth = path.PathWidth;
            tmpPath.StorageWidth = path.StorageWidth;
            tmpPath.Store = path.Store;
            tmpPath.StoreId = path.StoreId;
            tmpPath.Unilateral = path.Unilateral;
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Updates the specified row.
        /// </summary>
        /// <param name="row">The row which we want to update</param>
        public void UpdateRow(Row row)
        {
            ValidateRow(row);
            Row tmpRow = _contextDb.Rows.SingleOrDefault(x => x.Id == row.Id);
            tmpRow.Name = row.Name;
            tmpRow.Path = row.Path;
            tmpRow.PathId = row.PathId;
            tmpRow.Width = row.Width;
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Updates the specified 
        /// </summary>
        /// <param name="position"></param>
        public void UpdatePosition(Position position)
        {
            ValidatePosition(position);
            Position tmpPosition = _contextDb.Positions.SingleOrDefault(x => x.Id == position.Id);
            tmpPosition.Name = position.Name;
            tmpPosition.Row = position.Row;
            tmpPosition.RowId = position.RowId;
            tmpPosition.SupportType = position.SupportType;
            _contextDb.SaveChanges();
        }

        public void UpdateFloor(Floor floor)
        {
            ValidateFloor(floor);
            Floor tmpFloor = _contextDb.Floors.SingleOrDefault(x => x.Id == floor.Id);
            tmpFloor.AddressType = floor.AddressType;
            tmpFloor.PositionId = floor.PositionId;
            tmpFloor.Value = floor.Value;
            _contextDb.SaveChanges();
        }

        public void UpdateSupportType(SupportType supportType)
        {
            ValidateSupportType(supportType);
            SupportType tmpSupportType = _contextDb.SupportTypes.SingleOrDefault(x => x.Id == supportType.Id);
            tmpSupportType.Height = supportType.Height;
            tmpSupportType.Length = supportType.Length;
            tmpSupportType.Name = supportType.Name;
            tmpSupportType.Width = supportType.Width;
            _contextDb.SaveChanges();
        }

        public void UpdateProduct(Product product)
        {
            ValidateProduct(product);
            Product productTmp = _contextDb.Products.SingleOrDefault(pdt => pdt.Id == product.Id);
            productTmp.IsActive = product.IsActive;
            productTmp.Wording = product.Wording;
            _contextDb.SaveChanges();
        }

        public void UpdateProductFloor(ProductFloor productFloor)
        {
            ProductFloor productFloorTmp = _contextDb.ProductFloors.FirstOrDefault(pdtFlr => pdtFlr.FloorId == productFloor.FloorId && pdtFlr.ProductId == productFloor.ProductId);
            // For the special case of Internal Movements
            if(productFloorTmp == null)
            {
                productFloorTmp.Floor = productFloor.Floor;
                productFloorTmp.FloorId = productFloor.FloorId;
                productFloorTmp.Product = productFloor.Product;
                productFloorTmp.ProductId = productFloor.ProductId;
            }
            productFloorTmp.Barcode = productFloor.Barcode;
            productFloorTmp.CustomsNumber = productFloor.CustomsNumber;
            productFloorTmp.ExpirationDate = productFloor.ExpirationDate;
            productFloorTmp.LotNumber = productFloor.LotNumber;
            productFloorTmp.Quantity = productFloor.Quantity;
            productFloorTmp.ReceptionDate = productFloor.ReceptionDate;
            productFloorTmp.SerialNumber = productFloor.SerialNumber;
            productFloorTmp.Stored = productFloor.Stored;
            _contextDb.SaveChanges();
        }

        public void UpdateOrderProduct(OrderProduct orderProduct)
        {
            OrderProduct orderProductTmp = _contextDb.OrderProducts.SingleOrDefault(orderP => orderP.OrderId == orderProduct.OrderId && orderP.ProductId == orderProduct.ProductId);
            orderProductTmp.Quantity = orderProduct.Quantity;
            _contextDb.SaveChanges();
        }

        public void UpdatePacket(Packet packet, List<Tuple<Product, int>> products)
        {
            if (products == null || products.Count < 1)
                throw new ShowableException("Un colis doit contenir au moins un article.");
            ValidatePacket(packet);
            Packet packetToUpdate = _contextDb.Products.OfType<Packet>().SingleOrDefault(pck => pck.Id == packet.Id);
            if (packetToUpdate == null)
                throw new ShowableException("Le colis spécifié n'existe pas dans la base de données.");
            ValidatePacket(packet);
            List<PacketArticle> packetArticlestoDelete = new List<PacketArticle>();
            foreach (PacketArticle packetArticle in packet.Articles)
            {
                if (products.All(prod => prod.Item1.Id != packetArticle.ArticlesId))
                {
                    PacketArticle toDelete = _contextDb.PacketArticles.SingleOrDefault(pa =>
                        pa.PacketId == packetArticle.PacketId && pa.ArticlesId == packetArticle.ArticlesId);
                    packetArticlestoDelete.Add(toDelete);
                    _contextDb.PacketArticles.Remove(toDelete);
                }
                else
                {
                    packetArticle.Quantity = products.Single(prod => prod.Item1.Id == packetArticle.ArticlesId).Item2;
                }
            }

            foreach (Tuple<Product, int> product in products)
            {
                if (packet.Articles.All(pa => pa.ArticlesId != product.Item1.Id))
                {
                    PacketArticle toCreate = new PacketArticle() { ArticlesId = product.Item1.Id, PacketId = packet.Id, Quantity = product.Item2 };
                    _contextDb.PacketArticles.Add(toCreate);
                }
            }
            //packetToUpdate.Articles = packet.Articles;
            packetToUpdate.IsActive = packet.IsActive;
            packetToUpdate.ProductFloors = packet.ProductFloors;
            packetToUpdate.Wording = packet.Wording;
            _contextDb.SaveChanges();

        }
        #endregion

        #region Delete

        /// <summary>
        /// Deletes one of the products
        /// </summary>
        /// <param name="product">Prodcut which you want to delete</param>
        public void DeleteProduct(Product product)
        {
            if (product == null)
                throw new ShowableException("Le produit n'existe pas dans la base de données.");
            if (GetAddresses(product.Id, true).Any())
                throw new ShowableException("Le produit est présent dans l'entrepôt avec du stockage. Impossible de le supprimer.");
            DeleteProductFloors(product);
            product.IsActive = false;
            UpdateProduct(product);
        }



        /// <summary>
        /// Deletes one depository and all associated stores.
        /// </summary>
        /// <param name="depository"></param>
        public void DeleteDepository(Depository depository)
        {
            if (depository != null)
            {
                if (!GetAddresses(depository, true).Any())
                {
                    List<Store> storesAsociated = new List<Store>(_contextDb.Stores.Where(x => x.DepositoryId == depository.Id));
                    foreach (Store store in storesAsociated)
                        DeleteStore(store);
                    _contextDb.Depositories.Remove(_contextDb.Depositories.SingleOrDefault(x => x.Id == depository.Id));
                }
                _contextDb.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes one store and all associated paths.
        /// </summary>
        /// <param name="store"></param>
        public void DeleteStore(Store store)
        {
            if (store != null)
            {
                if (!GetAddresses(store, true).Any())
                {
                    List<Path> pathsAssociated = new List<Path>(_contextDb.Paths.Where(x => x.StoreId == store.Id));
                    foreach (Path path in pathsAssociated)
                        DeletePath(path);
                    _contextDb.Stores.Remove(_contextDb.Stores.SingleOrDefault(x => x.Id == store.Id));
                }
                _contextDb.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes one path and all associated rows.
        /// </summary>
        /// <param name="path"></param>
        public void DeletePath(Path path)
        {
            if (path != null)
            {
                if (!GetAddresses(path, true).Any())
                {
                    List<Row> rowsAssociated = new List<Row>(_contextDb.Rows.Where(x => x.PathId == path.Id));
                    foreach (Row row in rowsAssociated)
                        DeleteRow(row);
                    _contextDb.Paths.Remove(_contextDb.Paths.SingleOrDefault(x => x.Id == path.Id));
                }
                _contextDb.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes one row and all associated positions.
        /// </summary>
        /// <param name="row"></param>
        public void DeleteRow(Row row)
        {
            if (row != null)
            {
                if (!GetAddresses(row, true).Any())
                {
                    List<Position> positionsAssociated = new List<Position>(_contextDb.Positions.Where(x => x.RowId == row.Id));
                    foreach (Position positon in positionsAssociated)
                        DeletePosition(positon);
                    _contextDb.Rows.Remove(_contextDb.Rows.SingleOrDefault(x => x.Id == row.Id));
                }
                _contextDb.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes one position and all associated floors.
        /// </summary>
        /// <param name="position"></param>
        public void DeletePosition(Position position)
        {

            if (position != null)
            {
                if (!GetAddresses(position, true).Any())
                {
                    List<Floor> floorsAssociated = new List<Floor>(_contextDb.Floors.Where(x => x.PositionId == position.Id));
                    foreach (Floor floor in floorsAssociated)
                    {
                        if (_contextDb.ProductFloors.Any(pf => pf.Product.Id == floor.Id))
                            DeleteProductFloor(floor);
                        DeleteFloor(floor);
                    }
                    _contextDb.Positions.Remove(_contextDb.Positions.SingleOrDefault(x => x.Id == position.Id));
                }
                _contextDb.SaveChanges();
            }
        }

        /// <summary>
        /// Deletes one floor.
        /// </summary>
        /// <param name="floor"></param>
        public void DeleteFloor(Floor floor)
        {
            _contextDb.Floors.Remove(_contextDb.Floors.SingleOrDefault(x => x.Id == floor.Id));
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Deletes the productFloor link to the floor passed in parameter
        /// </summary>
        /// <param name="floor">The floor wich you want to delete the linked productFloor</param>
        public void DeleteProductFloor(Floor floor)
        {
            ProductFloor productFloor = _contextDb.ProductFloors.SingleOrDefault(x => x.FloorId == floor.Id);
            DeleteProductFloor(productFloor);

        }

        /// <summary>
        /// Delete one orderProduct and it order if this orderProduct is it last.
        /// </summary>
        /// <param name="orderProduct"></param>
        public void DeleteOrderProduct(OrderProduct orderProduct)
        {
            OrderProduct orderProductTmp = _contextDb.OrderProducts.SingleOrDefault(orderP => orderP.OrderId == orderProduct.OrderId && orderP.ProductId == orderProduct.ProductId);
            Order orderTmp = orderProductTmp.Order;
            _contextDb.OrderProducts.Remove(orderProductTmp);
            if (!_contextDb.OrderProducts.Any(op => op.OrderId == orderTmp.Id))
                DeleteOrder(orderTmp);
        }

        /// <summary>
        /// Permis to declare the order is prepared.
        /// </summary>
        /// <param name="order"></param>
        public void DeleteOrder(Order order)
        {
            Order orderTmp = _contextDb.Orders.SingleOrDefault(ord => ord.Id == order.Id);
            orderTmp.Prepared = OrderState.Prepared;
            _contextDb.SaveChanges();
        }

        /// <summary>
        /// Dele
        /// </summary>
        /// <param name="productFloor"></param>
        public void DeleteProductFloor(ProductFloor productFloor)
        {
            if (productFloor.Quantity > 0)
                throw new ShowableException("L'emplacement n'est pas vide, impossible de le supprimer ou de supprimer le produit stocké");
            _contextDb.ProductFloors.Remove(_contextDb.ProductFloors.SingleOrDefault(x => x.Floor.Id == productFloor.Floor.Id && x.Product.Id == productFloor.Product.Id));
            _contextDb.SaveChanges();
        }

        private void DeleteProductFloors(Product product)
        {
            List<ProductFloor> productFloors = _contextDb.ProductFloors.Where(x => x.Product.Id == product.Id).ToList();
            foreach (ProductFloor productFloor in productFloors)
                DeleteProductFloor(productFloor);
        }

        public void DeleteSupportType(SupportType supportType)
        {
            if (!_contextDb.Positions.Any(x => x.SupportType == supportType))
            {
                _contextDb.SupportTypes.Remove(_contextDb.SupportTypes.SingleOrDefault(x => x.Id == supportType.Id));
                _contextDb.SaveChanges();
            }
        }

        public void DeletePacket(Packet packet)
        {
            Packet toDelete = _contextDb.Products.OfType<Packet>().SingleOrDefault(pack => pack.Id == packet.Id);
            if (toDelete == null)
                throw new ShowableException("Le colis que vous voulez supprimer n'existe pas dans la base de données.");
            toDelete.IsActive = false;
            _contextDb.SaveChanges();
        }

        public bool DeleteProductFloorsNoStocked(String codebarreArticle, String codebarreAddress, List<ProductFloor> ShowStorage, List<ProductFloor> AllStorage,int count)
        {
            if (codebarreArticle == ShowStorage.First().Barcode.ToString()
                && codebarreAddress == ShowStorage.First().Floor.Position.Barcode)
            {
                ProductFloor productFloor = ShowStorage.First();
                ShowStorage.Remove(productFloor);
                productFloor.Stored = true;
                UpdateProductFloor(productFloor);
                if (ShowStorage.Count == 0)
                {
                    AllStorage = GetRegisteredProducts();
                    if (AllStorage.Count > count)
                        ShowStorage = AllStorage.GetRange(0, count);
                    else
                        ShowStorage = new List<ProductFloor>(AllStorage);
                }
                return true;
            }
            else
                return false;
        }

        public void DeactivateProduct(Product product)
        {
            if (product != null)
            {
                if (_contextDb.ProductFloors.Any(pf => pf.ProductId == product.Id))
                {
                    product.IsActive = false;
                    DeleteProduct(product);
                }
            }
        }

        #endregion

    }
}
