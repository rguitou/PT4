﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Appli.Model
{
    [DebuggerDisplay("Id={Id};Name={Name};PathId={PathId};Width={Width};PairSide={PairSide}")]
    public class Row
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// The width of the row in centimeters
        /// </summary>
        [Required]
        [Range(0,int.MaxValue)]
        public int Width { get; set; }

        /// <summary>
        /// The name associated to the row
        /// </summary>
        [Required]
        [Range(0, int.MaxValue)]
        public int Name { get; set; }

        /// <summary>
        /// The parent path
        /// </summary>
        [ForeignKey(nameof(Path))]
        public int PathId { get; set; }
        [ForeignKey(nameof(PathId))]
        public Path Path { get; set; }

        /// <summary>
        /// Specifies if the row is on the pair side of his path, it is always false if the path is unilateral
        /// </summary>
        [Required]
        public bool PairSide { get; set; }


    }
}
