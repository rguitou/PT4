﻿using Appli.Model;
using Appli.Model.Class;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Appli.Views
{
    /// <summary>
    /// Logique d'interaction pour InternalsMovements.xaml
    /// </summary>
    public partial class InternalsMovements

    {
        /// <summary>
        /// List of the movements showed in the DataGridView
        /// </summary>
        private List<InternalMovement> _movementsShowed;
        /// <summary>
        /// List of all the possible movements.
        /// </summary>
        private List<InternalMovement> _movements;

        /// <summary>
        /// List of the depositories
        /// </summary>
        private List<Depository> _depositories;
        /// <summary>
        /// List of the stores in the selected depository.
        /// </summary>
        private List<Store> _stores;
        /// <summary>
        /// List of the paths in the selected store.
        /// </summary>
        private List<Path> _paths;
        /// <summary>
        /// List of the rows in the selected path.
        /// </summary>
        private List<Row> _rows;
        /// <summary>
        /// List of the positions in the selected row.
        /// </summary>
        private List<Position> _positions;
        /// <summary>
        /// List of the floors in the selected row.
        /// </summary>
        private List<Floor> _floors;
        private string _numArticle;
        private string _numDouane;
        private string _numClient;
        private string _numReception;
        private string _depositorie;
        private string _store;
        private string _path;
        private string _row;
        private string _position;
        private string _floor;

        private List<string> _arguments;

        private string uiSearchToggleStringBefore = "Avant la recherche";
        private string uiSearchToggleStringAfter = "Après la recherche";

        private bool _utilisationCombobox;

        public bool UtilisationCombobox
        {
            get { return _utilisationCombobox; }
            set
            {
                if (value == _utilisationCombobox) return;
                _utilisationCombobox = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// Getter and Setter of _depositories
        /// </summary>
        public List<Depository> Depositories
        {
            get { return _depositories; }
            set
            {
                if (value == _depositories) return;
                _depositories = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _stores
        /// </summary>
        public List<Store> Stores
        {
            get { return _stores; }
            set
            {
                if (value == _stores) return;
                _stores = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _paths
        /// </summary>
        public List<Path> Paths
        {
            get { return _paths; }
            set
            {
                if (value == _paths) return;
                _paths = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _rows
        /// </summary>
        public List<Row> Rows
        {
            get { return _rows; }
            set
            {
                if (value == _rows) return;
                _rows = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _positions
        /// </summary>
        public List<Position> Positions
        {
            get { return _positions; }
            set
            {
                if (value == _positions) return;
                _positions = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _floors
        /// </summary>
        public List<Floor> Floors
        {
            get { return _floors; }
            set
            {
                if (value == _floors) return;
                _floors = value;
                OnPropertyChanged();
            }
        }


        /// <summary>
        /// Getter and Setter of _movements
        /// </summary>
        public List<InternalMovement> Movements
        {
            get { return _movements; }
            set
            {
                if (value == _movements) return;
                _movements = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Getter and Setter of _movementsShowed
        /// </summary>
        public List<InternalMovement> MovementsShowed
        {
            get { return _movementsShowed; }
            set
            {
                if (value == _movementsShowed) return;
                _movementsShowed = value;
                OnPropertyChanged();
            }
        }

        private void ChangeVisibility(Label label, ComboBox combo, bool visible)
        {
            if (visible)
            {
                label.Visibility = Visibility.Visible;
                combo.Visibility = Visibility.Visible;
            }
            else
            {
                label.Visibility = Visibility.Hidden;
                combo.Visibility = Visibility.Hidden;
                combo.SelectedItem = null;
            }
        }

        private void ChangeVisibility(ComboBox combo, bool visible)
        {
            if (visible)
            {
                combo.Visibility = Visibility.Visible;
            }
            else
            {
                combo.Visibility = Visibility.Hidden;
                combo.SelectedItem = null;
            }
        }

        /// <summary>
        /// Function dealing each changes in the ComboBoxs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void IndexChanged(object sender, SelectionChangedEventArgs e)
        {
            // Update choices of Stores
            if (DepositoriesCb.SelectedItem != null && StoresCb.SelectedItem == null)
            {
                ChangeVisibility(StoresCb, true);
                Stores = Globals.Model.GetStores((Depository)DepositoriesCb.SelectedItem);
            }
            // Update choices of Paths
            if (StoresCb.SelectedItem != null && PathsCb.SelectedItem == null)
            {
                ChangeVisibility(PathsCb, true);
                Paths = Globals.Model.GetPaths((Store)StoresCb.SelectedItem);
            }
            // Update choices of Rows
            if (PathsCb.SelectedItem != null)
            {
                ChangeVisibility(RowsCb, true);
                Rows = Globals.Model.GetRows((Path)PathsCb.SelectedItem);
            }
            // Update choices of Positions
            if (RowsCb.SelectedItem != null)
            {
                ChangeVisibility(PositionsCb, true);
                Positions = Globals.Model.GetPositions((Row)RowsCb.SelectedItem);
            }
            // Update choices of Floors
            if (PositionsCb.SelectedItem != null)
            {
                ChangeVisibility(FloorsCb, true);
                Floors = Globals.Model.GetFloors((Position)PositionsCb.SelectedItem);
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public InternalsMovements()
        {
            // Initialization of the ComboBoxs
            InitializeComponent();
            DepositoriesCb.ItemsSource = Globals.Model.GetDepositories();
            DepositoriesCb.DisplayMemberPath = "Name";
            MovementsShowed = new List<InternalMovement>();
            Movements = new List<InternalMovement>();

            //Initialization of the Lists
            _numArticle = "";
            _numDouane = "";
            _numClient = "";
            _numReception = "";
            _depositorie = "";
            _store = "";
            _path = "";
            _row = "";
            _position = "";
            _floor = "";

            Depositories = new List<Depository>();
            Stores = new List<Store>();
            Paths = new List<Path>();
            Rows = new List<Row>();
            Positions = new List<Position>();
            Floors = new List<Floor>();


            _arguments = new List<string>();

            _arguments.Add(_numArticle);
            _arguments.Add(_numDouane);
            _arguments.Add(_numClient);
            _arguments.Add(_numReception);
            _arguments.Add(_depositorie);
            _arguments.Add(_store);
            _arguments.Add(_path);
            _arguments.Add(_row);
            _arguments.Add(_position);
            _arguments.Add(_floor);

            Exec.IsChecked = false;
        }

        /// <summary>
        /// Function which permits to select one internal movement.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridTextColumn_Click(object sender, RoutedEventArgs e)
        {
            InternalMovement mvmtSelected = (InternalMovement)sender;
            GridMovements.Items.Refresh();
        }

        private void reinit_Click(object sender, RoutedEventArgs e)
        {
            _numArticle = "";
            _numDouane = "";
            _numClient = "";
            _numReception = "";
            _depositorie = "";
            _store = "";
            _path = "";
            _row = "";
            _position = "";
            _floor = "";
            _arguments = new List<string>();

            _arguments.Add(_numArticle);
            _arguments.Add(_numDouane);
            _arguments.Add(_numClient);
            _arguments.Add(_numReception);
            _arguments.Add(_depositorie);
            _arguments.Add(_store);
            _arguments.Add(_path);
            _arguments.Add(_row);
            _arguments.Add(_position);
            _arguments.Add(_floor);

            Article.Text = "";
            Douane.Text = "";
            Client.Text = "";
            Reception.Text = "";
            DepositoriesCb.SelectedItem = null;
            ChangeVisibility(StoresCb, false);
            ChangeVisibility(PathsCb, false);
            ChangeVisibility(RowsCb, false);
            ChangeVisibility(PositionsCb, false);
            ChangeVisibility(FloorsCb, false);
        }
        /// <summary>
        /// Gets the argmuents back and calculates the possibles movements.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void search_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DepositoriesCb.SelectedItem != null)
                {
                    _depositorie = DepositoriesCb.Text;
                    _arguments[4] = _depositorie;
                    if (StoresCb.SelectedItem != null)
                    {
                        _store = StoresCb.Text;
                        _arguments[5] = _store;
                        if (PathsCb.SelectedItem != null)
                        {
                            _path = PathsCb.Text;
                            _arguments[6] = _path;
                            if (RowsCb.SelectedItem != null)
                            {
                                _row = RowsCb.Text;
                                _arguments[7] = _row;
                                if (PositionsCb.SelectedItem != null)
                                {
                                    _position = PositionsCb.Text;
                                    _arguments[8] = _position;
                                    if (FloorsCb.SelectedItem != null)
                                    {
                                        _floor = FloorsCb.Text;
                                        _arguments[9] = _floor;
                                    }
                                }
                            }
                        }
                    }
                }
                // Try parsing the values entered by the user
                if (FilterValidation(Article.Name, Article.Text, ErrorMessage) &&
                    FilterValidation(Douane.Name, Douane.Text, ErrorMessage) &&
                    FilterValidation(Client.Name, Client.Text, ErrorMessage) &&
                    FilterValidation(Reception.Name, Reception.Text, ErrorMessage))
                {
                    _arguments[0] = Article.Text;
                    _arguments[1] = Douane.Text;
                    _arguments[2] = Client.Text;
                    _arguments[3] = Reception.Text;
                    ErrorMessage.Text = "";
                    if (Globals.Model.GetMovements(_arguments, UtilisationCombobox).Count > 0)
                    {
                        Movements = Globals.Model.GetMovements(_arguments, UtilisationCombobox);
                    }
                    else
                        ErrorMessage.Text = "Aucun résultat pour cette recherche.";
                }
                ChangeMovementsShowed();

            }
            catch (FormatException)
            {
                ErrorMessage.Text = "Un ou des Champ(s) sont invalides";
            }
            GridMovements.Items.Refresh();
        }

        /// <summary>
        /// Permits to show ony validated movements.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Exec_Click(object sender, RoutedEventArgs e)
        {
            if ((bool)Change.IsChecked)
                FilterSearchLabel.Content = "Filtrage : " + uiSearchToggleStringAfter;
            else FilterSearchLabel.Content = "Filtrage : " + uiSearchToggleStringBefore;
            ChangeMovementsShowed();
            GridMovements.Items.Refresh();
        }

        /// <summary>
        /// Update the lists to show only validated movements.
        /// </summary>
        private void ChangeMovementsShowed()
        {
            if (Exec.IsChecked == true)
            {
                MovementsShowed.Clear();
                foreach (InternalMovement intMov in Movements)
                {
                    if (intMov.Validate)
                        MovementsShowed.Add(intMov);
                }
            }
            else
                MovementsShowed = new List<InternalMovement>(Movements);
        }

        /// <summary>
        /// Called when the movement is done. Update associated productfloor.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Valider_Click(object sender, RoutedEventArgs e)
        {
            if(MovementsShowed.Count > 0)
            {
                if (CodebarreAddBegin.Text == MovementsShowed[0].StartingFloor.Position.Barcode
                && CodebarreAddFinal.Text == MovementsShowed[0].FinalFloor.Position.Barcode
                && CodebarreProduct.Text == MovementsShowed[0].ProductFloorConcerned.Barcode)
                {
                    MovementsShowed[0].ProductFloorConcerned.Floor = MovementsShowed[0].FinalFloor;
                    MovementsShowed[0].ProductFloorConcerned.FloorId = MovementsShowed[0].FinalFloor.Id;
                    
                    //Globals.Model.UpdateProductFloor(MovementsShowed[0].ProductFloorConcerned);
                    MovementsShowed.Remove(MovementsShowed[0]);
                    ErrorMessage.Text = "Le mouvement a bien été effectué";
                }
                else
                {
                    ErrorMessage.Text = "Les codes barres sont invalides";
                }
                GridMovements.Items.Refresh();
            }
        }
    }
}
