﻿using Appli.Model;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Storage.xaml
    /// </summary>
    public partial class Storage
    {
        public static int countOfProductFloors = 15;
        /// <summary>
        /// List of the products not stocked.
        /// </summary>
        private List<ProductFloor> _allStorage;

        /// <summary>
        /// Getter and Setter of _allStorage.
        /// </summary>
        public List<ProductFloor> AllStorage
        {
            get { return _allStorage; }
            set
            {
                if (value == _allStorage) return;
                _allStorage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the products not stocked and showed by the user.
        /// </summary>
        private List<ProductFloor> _showStorage;

        /// <summary>
        /// Getter and Setter of _showStorage.
        /// </summary>
        public List<ProductFloor> ShowStorage
        {
            get { return _showStorage; }
            set
            {
                if (value == _showStorage) return;
                _showStorage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Constructor of Storage
        /// </summary>
        public Storage()
        {
            InitializeComponent();
            AllStorage = Globals.Model.GetRegisteredProducts();
            if (AllStorage.Count > countOfProductFloors)
                ShowStorage = AllStorage.GetRange(0, countOfProductFloors);
            else
                ShowStorage = new List<ProductFloor>(AllStorage);
        }

        /// <summary>
        ///  Called when a product is stocked. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Stocker_Click(object sender, RoutedEventArgs e)
        {
            if (FilterValidation(CodebarreArticle.Name, CodebarreArticle.Text, ErrorMessage) 
                && FilterValidation(CodebarreAdresse.Name, CodebarreAdresse.Text, ErrorMessage))
            {
                ErrorMessage.Text = "";
                bool result = Globals.Model.DeleteProductFloorsNoStocked(CodebarreArticle.Text, CodebarreAdresse.Text, ShowStorage, AllStorage,countOfProductFloors);
                GridStorage.Items.Refresh();
                if (result)
                {
                    ErrorMessage.Text = "Operation réussie";
                }
                else
                    ErrorMessage.Text = "Operation échouée";
            }

        }
    }
}
