﻿using Appli.Model;
using Appli.Model.Class;
using Appli.Model.Class.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Articles.xaml
    /// </summary>
    public partial class Preparation
    {
        /// <summary>
        /// Order which is not prepared
        /// </summary>
        private CustomerOrder _nextNextOrder;
        public CustomerOrder NextOrder
        {
            get { return _nextNextOrder; }
            set
            {
                if (value == _nextNextOrder) return;
                _nextNextOrder = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the productfloors showed in the DataGridVIew
        /// </summary>
        private List<ProductFloor> _productFloorsShowable;

        /// <summary>
        /// Getter and Setter of _productFloorsShowable.
        /// </summary>
        public List<ProductFloor> ProductFloorsShowable
        {
            get { return _productFloorsShowable; }
            set
            {
                if (value == _productFloorsShowable) return;
                _productFloorsShowable = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// List of the possible ProductFloors which will maybe used for the preparation. 
        /// </summary>
        private List<ProductFloor> _productFloors;

        /// <summary>
        /// Getter and Setter of _productFloors.
        /// </summary>
        public List<ProductFloor> ProductFloors
        {
            get { return _productFloors; }
            set
            {
                if (value == _productFloors) return;
                _productFloors = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Constructor of Preparation
        /// </summary>
        public Preparation()
        {
            NextOrder = Globals.Model.GetNextOrder();
            InitializeComponent();
            ProductFloors = new List<ProductFloor>();
        }

        /// <summary>
        /// Search the product in the specified order.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            bool articlesAvailables = true;
            Product productNotAvailable = null;
            ProductFloors.Clear();
            try
            {
                if (FilterValidation(CbCommande.Name, CbCommande.Text, MessageStart))
                {
                    List<OrderProduct> orderProducts = new List<OrderProduct>();
                    orderProducts = Globals.Model.GetOrderProducts().Where(op => op.Order.Barcode == CbCommande.Text).ToList();
                    foreach (OrderProduct op in orderProducts)
                    {
                        ProductFloor productFloor = Globals.Model.GetProductFloor(op.Product, op.Quantity, new List<ProductFloor>());
                        if (productFloor == null)
                        {
                            articlesAvailables = false;
                            productNotAvailable = op.Product;
                            break;
                        }
                        else
                        {
                            int qtt = productFloor.Quantity;
                            do
                            {
                                ProductFloors.Add(productFloor);
                                productFloor = Globals.Model.GetProductFloor(op.Product, op.Quantity, ProductFloors);
                                qtt += productFloor.Quantity;
                            }
                            while (qtt < op.Quantity && productFloor != null);
                        }

                    }
                    if (articlesAvailables)
                    {
                        MessageStart.Text = "";
                        ProductFloorsShowable = new List<ProductFloor>(ProductFloors);
                        ProductFloorsShowable.OrderBy(x => x.Floor.Position.Row.Path.Store.Depository.Name)
                        .ThenBy(x => x.Floor.Position.Row.Path.Store.Name)
                        .ThenBy(x => x.Floor.Position.Row.Path.Name)
                        .ThenBy(x => x.Floor.Position.Name)
                        .ThenBy(x => x.Floor.Id);
                    }
                    else
                        MessageStart.Text = "Il n'y a plus de " + productNotAvailable.Wording + " en picking.";
                }
            }
            catch (FormatException)
            {
                MessageStart.Text = "Entrer un code barre de commande !";
            }
            GridPreparation.Items.Refresh();
        }

        /// <summary>
        /// Called when one produt of the order is collected. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Validate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FilterValidation(CbArticle.Name, CbArticle.Text, MessageOk) &&
                    FilterValidation(CbAddress.Name, CbAddress.Text, MessageOk))
                {
                    String codebarreProduit = CbArticle.Text;
                    String codebarreAddress = CbAddress.Text;
                    ProductFloor productFloor = ProductFloorsShowable.FirstOrDefault(pf => pf.Barcode == codebarreProduit && pf.Floor.Position.Barcode == codebarreAddress);
                    if (productFloor != null)
                    {
                        OrderProduct orderProduct = Globals.Model.GetOrderProducts().FirstOrDefault(op => op.Order.Barcode == CbCommande.Text && op.Product.Id == productFloor.Product.Id);
                        if (orderProduct.Quantity >= productFloor.Quantity)
                        {
                            Globals.Model.DeleteProductFloor(productFloor);
                            ProductFloorsShowable.Remove(productFloor);
                            Globals.Model.DeactivateProduct(productFloor.Product);
                            orderProduct.Quantity -= productFloor.Quantity;
                            if (orderProduct.Quantity > productFloor.Quantity)
                                Globals.Model.UpdateOrderProduct(orderProduct);
                            else
                                Globals.Model.DeleteOrderProduct(orderProduct);
                        }
                        else if (orderProduct.Quantity < productFloor.Quantity)
                        {
                            productFloor.Quantity -= orderProduct.Quantity;
                            Globals.Model.UpdateProductFloor(productFloor);
                            Globals.Model.DeleteOrderProduct(orderProduct);
                        }
                    }
                    else
                        MessageOk.Text = "Les codes barre ne correspondent pas.";
                }
            }
            catch (FormatException)
            {
                MessageOk.Text = "Entrer un code barre d'adresse et de produit !";
            }
            catch (ArgumentNullException)
            {
                MessageOk.Text = "Ce code barre d'adresse et/ou de produit n'est pas présent dans la commande!";
            }
            catch (NullReferenceException)
            {
                MessageOk.Text = "Ce code barre d'adresse et/ou de produit n'est pas présent dans la commande!";
            }
            NextOrder = Globals.Model.GetNextOrder();
            GridPreparation.Items.Refresh();
        }

    }
}
