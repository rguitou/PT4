﻿using System;
using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;
using System.Windows.Controls;
using Appli.Model.Class.UI;
using MaterialDesignThemes.Wpf;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class AddProductPacketForm
    {
        private Packet _packet;

        public Packet Packet
        {
            get { return _packet; }
            set
            {
                if (value == _packet) return;
                _packet = value;
                OnPropertyChanged();
            }
        }

        private List<Product> _products;

        public List<Product> Products
        {
            get { return _products; }
            set
            {
                if (value == _products) return;
                _products = value;
                OnPropertyChanged();
            }
        }

        private List<Product> _showableProducts;

        public List<Product> ShowableProducts
        {
            get { return _showableProducts; }
            set
            {
                if (value == _showableProducts) return;
                _showableProducts = value;
                OnPropertyChanged();
            }
        }

        private bool _creation;

        public bool Creation
        {
            get { return _creation; }
            set
            {
                if (value == _creation) return;
                _creation = value;
                OnPropertyChanged();
            }
        }


        public List<Tuple<Product, int>> PacketArticles { get; set; }

        public AddProductPacketForm(Packet packet, List<Tuple<Product, int>> packetArticles, Product article = null)
        {
            InitializeComponent();
            Packet = new Packet(packet);
            PacketArticles = packetArticles;
            if (article != null)
            {
                Products = new List<Product>();
                Products.Add(article);
                FillShowableProducts();
                LbProducts.SelectedIndex = 0;
                TbQuantity.Text = packetArticles.Single(pa => pa.Item1.Id == article.Id).Item2.ToString();
            }
            else
            {
                Products = Globals.Model.GetProductsToAddInPacket(packet);
                Creation = true;
                FillShowableProducts();

            }
        }


        private void BackToParent(Tuple<Product, int> product)
        {
            if (!DialogSession.IsEnded)
                DialogSession.Close(product);
        }

        private void FillShowableProducts()
        {
            if (!string.IsNullOrWhiteSpace(TbSearch.Text))
            {
                List<Product> products = Products.Where(prod =>
                    prod.Wording.ToLower().Contains(TbSearch.Text.ToLower()) || prod.Id.ToString().StartsWith(TbSearch.Text)).ToList();
                if (products.Count > 10)
                    ShowableProducts = products.GetRange(0, 10);
                else
                    ShowableProducts = products;
            }
            else
            {
                if (Products.Count > 10)
                    ShowableProducts = Products.GetRange(0, 10);
                else
                    ShowableProducts = Products;
            }

        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            BackToParent(null);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (LbProducts.SelectedItem != null)
                {
                    int quantity = -1;
                    int.TryParse(TbQuantity.Text, out quantity);
                    if (quantity > 0)
                    {
                        BackToParent(new Tuple<Product, int>((Product)LbProducts.SelectedItem, quantity));
                    }
                    else
                    {
                        Errors = "La quantité doit être un nombre supérieur ou égale à 1.";
                    }
                }
                else
                {
                    Errors = "Veuillez sélectionner un produit dans la liste.";
                }
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void TbSearch_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            FillShowableProducts();
        }

        private void BtnDelete_OnClicke_Click(object sender, RoutedEventArgs e)
        {
            BackToParent(new Tuple<Product, int>((Product)LbProducts.SelectedItem, 0));
        }
    }
}
