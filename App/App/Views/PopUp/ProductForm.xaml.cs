﻿using System;
using System.Collections.Generic;
using System.Linq;
using Appli.Model;
using Appli.Model.Class.Exception;
using System.Windows;
using System.Windows.Controls;
using Appli.Model.Class.UI;
using MaterialDesignThemes.Wpf;

namespace Appli.Views.PopUp
{
    /// <summary>
    /// Interaction logic for DepositoryForm.xaml
    /// </summary>
    public partial class ProductForm
    {
        private Product _produit;

        public Product Product
        {
            get { return _produit; }
            set
            {
                if (value == _produit) return;
                _produit = value;
                OnPropertyChanged();
            }
        }

        private List<Tuple<Product, int>> _products;

        public List<Tuple<Product, int>> Products
        {
            get { return _products; }
            set
            {
                if (value == _products) return;
                _products = value;
                OnPropertyChanged();
            }
        }

        private Type _type;

        private bool _creation;

        private bool _isPopUp = true;

        private bool _isPacket;

        public bool IsPacket
        {
            get { return _isPacket; }
            set
            {
                if (value == _isPacket) return;
                _isPacket = value;
                OnPropertyChanged();
            }
        }


        public ProductForm(Product product)
        {
            InitializeComponent();

            if (product.GetType() == typeof(Packet))
            {
                LabelTitle.Content = "Colis";
                Product = new Packet((Packet)product);
                Products = new List<Tuple<Product, int>>();
                foreach (PacketArticle packetArticle in ((Packet)Product).Articles)
                {
                    Products.Add(new Tuple<Product, int>(packetArticle.Article, packetArticle.Quantity));
                }
                IsPacket = true;
            }
            else
            {
                LabelTitle.Content = "Article";
                Product = new Product(product);
            }
        }


        public ProductForm(Product product, Type type) : this(product)
        {
            _isPopUp = false;
            _type = type;
        }

        public ProductForm(bool forPacket)
        {
            InitializeComponent();

            Product = forPacket ? new Packet() : new Product();
            Products = new List<Tuple<Product, int>>();
            _creation = true;

            IsPacket = forPacket;
        }

        public ProductForm(bool forPacket, Type type) : this(forPacket)
        {
            _isPopUp = false;
            _type = type;
        }

        private void BackToParent()
        {
            if (_isPopUp)
            {
                if (!DialogSession.IsEnded)
                    DialogSession.Close();
            }
            else
            {
                AppliUserControl userControl = Globals.MainWindow.UserControls.Single(uc => uc.GetType() == _type);
                if (_type == typeof(Articles))
                    ((Articles)userControl).OnReOpen();
                else if (_type == typeof(Preparation))
                    ((Reception)userControl).OnReOpen();
                Globals.MainWindow.CcPrincipal.Content = userControl;
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {

            BackToParent();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Product.GetType() == typeof(Packet))
                {
                    if (_creation)
                        Globals.Model.CreatePacket((Packet)Product, Products);
                    else
                        Globals.Model.UpdatePacket((Packet)Product, Products);
                }
                else
                {
                    if (_creation)
                        Globals.Model.CreateProduct(Product);
                    else
                        Globals.Model.UpdateProduct(Product);

                }

                BackToParent();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Voulez vous vraiment supprimer ce " + (Product.GetType() == typeof(Packet) ? "colis" : "produit") + " ?", "Attention",
                        MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes)
                    return;
                if (Product.GetType() == typeof(Packet))
                {
                    Globals.Model.DeletePacket((Packet)Product);
                }
                else
                {
                    Globals.Model.DeleteProduct(Product);
                }
                BackToParent();
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }
        }

        private void EventSetter_OnHandlerDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            AddProductPacketForm addProductPacketForm = new AddProductPacketForm((Packet)Product, Products, ((Tuple<Product, int>)((DataGridRow)sender).DataContext).Item1);
            Globals.OpenDialog(addProductPacketForm, OnPopUpClose);
        }

        private void OnPopUpClose(object sender, DialogClosingEventArgs eventArgs)
        {
            List<Tuple<Product, int>> productBis = new List<Tuple<Product, int>>(Products);
            Tuple<Product, int> prodQuantity = (Tuple<Product, int>)eventArgs.Parameter;
            if (prodQuantity != null)
            {
                Tuple<Product, int> product = Products.SingleOrDefault(prod => prod.Item1.Id == prodQuantity.Item1.Id);
                if (product != null)
                {
                    productBis.Remove(product);
                }
                if (prodQuantity.Item2 > 0)
                    productBis.Add(prodQuantity);
                Products = new List<Tuple<Product, int>>(productBis);
            }
        }

        private void BtnAdd_OnClick(object sender, RoutedEventArgs e)
        {
            AddProductPacketForm addProductPacketForm = new AddProductPacketForm((Packet)Product, Products);
            Globals.OpenDialog(addProductPacketForm, OnPopUpClose);
        }
    }
}
