﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Appli.Model;
using Appli.Model.Class.UI;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media.TextFormatting;
using Appli.Model.Class;
using Appli.Model.Class.Exception;
using Appli.Views.PopUp;
using MaterialDesignThemes.Wpf;
using Image = iTextSharp.text.Image;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Reception.xaml
    /// </summary>
    public partial class Reception
    {
        bool inputValid = false;
        bool traceabilityInputValid = false;
        private Product createdProduct;

        private List<Product> _products;

        public List<Product> Products
        {
            get { return _products; }
            set
            {
                if (value == _products) return;
                _products = value;
                OnPropertyChanged();
            }
        }

        private List<Product> _showableProducts;

        public List<Product> ShowableProducts
        {
            get { return _showableProducts; }
            set
            {
                if (value == _showableProducts)
                    return;
                _showableProducts = value;
                OnPropertyChanged();
            }
        }

        private List<Depository> _depositories;

        public List<Depository> Depositories
        {
            get { return _depositories; }
            set
            {
                if (value == _depositories)
                    return;
                _depositories = value;
                OnPropertyChanged();
            }
        }

        private List<SupportType> _supportTypes;

        public List<SupportType> SupportTypes
        {
            get { return _supportTypes; }
            set
            {
                if (value == _supportTypes)
                    return;
                _supportTypes = value;
                OnPropertyChanged();
            }
        }

        private bool _withTraceability;

        public bool WithTraceability
        {
            get { return _withTraceability; }
            set
            {
                if (value == _withTraceability)
                    return;
                _withTraceability = value;
                OnPropertyChanged();
            }
        }

        private bool _withImpression;

        public bool WithImpression
        {
            get { return _withImpression; }
            set
            {
                if (value == _withImpression)
                    return;
                Parameters.ReceptionPrintPreference = _withImpression = value;
                Parameters.Save();
                OnPropertyChanged();
            }
        }

        private DateTime? _receptionDate;

        public DateTime? ReceptionDate
        {
            get { return _receptionDate; }
            set
            {
                if (value == _receptionDate)
                    return;
                _receptionDate = value;
                OnPropertyChanged();
            }
        }

        private DateTime? _expirationDate;

        public DateTime? ExpirationDate
        {
            get { return _expirationDate; }
            set
            {
                if (value == _expirationDate)
                    return;
                _expirationDate = value;
                OnPropertyChanged();
            }
        }






        public Reception()
        {
            Products = Globals.Model.GetProducts();
            Depositories = Globals.Model.GetDepositories();
            SupportTypes = Globals.Model.GetSupportTypes();
            ReceptionDate = DateTime.Today;
            WithImpression = Parameters.ReceptionPrintPreference;
            InitializeComponent();
            FillShowableProduct();
        }

        private void FillShowableProduct()
        {
            if (string.IsNullOrWhiteSpace(TbSearch.Text))
                ShowableProducts = Products.Count > 10 ? Products.GetRange(0, 10) : Products;
            else
            {
                List<Product> products = Products.Where(product =>
                    product.Id.ToString().StartsWith(TbSearch.Text) || product.Wording.ToLowerInvariant()
                        .Contains(TbSearch.Text.ToLowerInvariant())).ToList();
                ShowableProducts = products.Count > 10 ? products.GetRange(0, 10) : products;
            }
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                validateInputs();
                Depository selectedDepository = (Depository)ComboDepositories.SelectedItem;
                if (selectedDepository == null)
                    throw new ShowableException("Vous n'avez sélectionné aucun dépot");
                //no need to get the depository from the database since it already comes from it
                Product selectedProduct = (Product)LbProducts.SelectedItem;

                if (selectedProduct == null)
                    throw new ShowableException(
                        "Aucun produit n'est sélectionné. Si le produit reçu n'existe pas, créez-le en utilisant le bouton \"Créer nouveau produit\" ou \"Créer nouvel article\" ");


                if (WithTraceability)
                    ReceiveWithTraceability(selectedDepository, selectedProduct);
                else
                {
                    Globals.Model.ReceiveProduct(selectedProduct, selectedDepository, out string barcode, null,
                        int.Parse(TbQuantity.Text));
                    if (WithImpression)
                        Print(barcode);
                }

                SnackbarValidation.MessageQueue.Enqueue("Le produit a bien été créé.");
            }
            catch (ShowableException se)
            {
                Errors = se.Message;
            }

        }

        private void OnPopUpClose(object sender, DialogClosingEventArgs eventArgs)
        {
            // TODO test popUp closing reason

            Products = Globals.Model.GetProducts();
            FillShowableProduct();


            Product tmp = ((ProductForm)eventArgs.Session.Content).Product;
            TbSearch.Text = tmp.Id.ToString();
            LbProducts.SelectedIndex = 0;
        }

        private void validateInputs()
        {
            int.TryParse(TbQuantity.Text, out var quantity);
            if (quantity < 1)
                throw new ShowableException("La quantité doit être au moins de 1.");
            if (WithTraceability)
            {
                if (!ReceptionDate.HasValue)
                    throw new ShowableException(
                        "La date de réception est obligatoire sur les produits avec traçabilité.");
                if (DateTime.Compare(ReceptionDate.Value, DateTime.Today) > 0)
                    throw new ShowableException("La date de réception ne peut pas être dans le futur.");
                if (!ExpirationDate.HasValue)
                    throw new ShowableException(
                        "La date d'expération est obligatoire sur les produits avec traçabilité.");
                if (DateTime.Compare(ExpirationDate.Value, DateTime.Today) <= 0)
                    throw new ShowableException("La date d'expiration doit être dans le futur.");
                if (TbCustomNumber.Text.Length != 13)
                    throw new ShowableException("Le numéro de douane doit faire 13 chiffres.");
                if (TbLotNumber.Text.Length != 13)
                    throw new ShowableException("Le numéro de lot doit faire 13 chiffres.");
                if (TbSerialNumber.Text.Length != 13)
                    throw new ShowableException("Le numéro de série doit faire 13 chiffres.");
            }
        }

        private void ReceiveWithTraceability(Depository selectedDepository, Product toReceive)
        {

            ProductFloor traceAbilityInfo = new ProductFloor()
            {
                Quantity = int.Parse(TbQuantity.Text),
                ReceptionDate = ReceptionDate.Value,
                ExpirationDate = ExpirationDate.Value,
                SerialNumber = TbSerialNumber.Text,
                CustomsNumber = TbCustomNumber.Text,
                LotNumber = TbLotNumber.Text,
                ProductId = toReceive.Id,
            };
            Globals.Model.ReceiveProduct(toReceive, selectedDepository, out string barcode, traceAbilityInfo,
                int.Parse(TbQuantity.Text));

            if (WithImpression)
            {
                Print(barcode);
            }
        }


        private void Print(string barcode)
        {
            StringBuilder sheet = Globals.Model.HelperGenerateReceptionLabelToPrint(barcode);
            if (sheet == null)
            {
                throw new ShowableException(
                    "Erreur lors de la génération de l'étiquette de réception. Code barre généré : " + barcode + ".");
            }

            SaveFileDialog saveFile = new SaveFileDialog() { Filter = "PDF file|*.pdf", ValidateNames = true };
            {
                if (saveFile.ShowDialog() == DialogResult.OK)
                {
                    Document document = new Document(PageSize.A4.Rotate());
                    FileStream fileStream = new FileStream(saveFile.FileName, FileMode.Create);
                    try
                    {

                        PdfWriter.GetInstance(document, fileStream);
                        document.Open();
                        document.Add(new Paragraph(sheet.ToString()));
                        Image pic = Image.GetInstance(Globals.Model.HelperGetGraphicBarcode(barcode),
                            System.Drawing.Imaging.ImageFormat.Jpeg);
                        document.Add(pic);
                        document.Add(new Paragraph(barcode));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw new ShowableException(
                            "Erreur lors de la génération de l'étiquette de réception. Code barre généré : " + barcode +
                            ".");
                    }
                    finally
                    {
                        document.Close();
                        fileStream.Close();
                    }
                    Process.Start(saveFile.FileName);
                }
            }
        }


        private void TbSearch_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            FillShowableProduct();
        }

        public void OnReOpen()
        {
            FillShowableProduct();
        }

        private void BtnNewProduct_OnClick(object sender, RoutedEventArgs e)
        {
            ProductForm productForm = new ProductForm(false);
            Globals.OpenDialog(productForm, OnPopUpClose);
        }

        private void BtnNewArticle_OnClick(object sender, RoutedEventArgs e)
        {
            ProductForm productForm = new ProductForm(false, GetType());
            Globals.MainWindow.CcPrincipal.Content = productForm;
        }
    }
}
