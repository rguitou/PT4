﻿using Appli.Model;
using Appli.Model.Class.UI;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Appli.Views.PopUp;
using MaterialDesignThemes.Wpf;

namespace Appli.Views
{
    /// <summary>
    /// Interaction logic for Articles.xaml
    /// </summary>
    public partial class Articles
    {

        private List<Product> _products;

        public List<Product> Products
        {
            get { return _products; }
            set
            {
                if (value == _products) return;
                _products = value;
                OnPropertyChanged();
            }
        }

        private List<Product> _showableProducts;

        public List<Product> ShowableProducts
        {
            get { return _showableProducts; }
            set
            {
                if (value == _showableProducts) return;
                _showableProducts = value;
                OnPropertyChanged();
            }
        }


        public Articles()
        {
            InitializeComponent();
            LoadProducts();
        }

        private void LoadProducts()
        {
            Products = Globals.Model.GetProducts(true, true);
            FillShowableProduct();
        }

        private void FillShowableProduct()
        {
            if (string.IsNullOrWhiteSpace(TbCodeArticle.Text))
                ShowableProducts = Products.Count > 30 ? Products.GetRange(0, 30) : Products;
            else
            {
                List<Product> products = Products.Where(product => product.Id.ToString().StartsWith(TbCodeArticle.Text) || product.ProductFloors.Any(pf => pf.Barcode.StartsWith(TbCodeArticle.Text))).ToList();
                ShowableProducts = products.Count > 30 ? products.GetRange(0, 30) : products;
            }
        }

        private void TbCodeArticle_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            FillShowableProduct();
        }

        private void EventSetter_OnHandlerDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Globals.MainWindow.CcPrincipal.Content = new ProductForm((Product)((DataGridRow)sender).DataContext, GetType());
        }

        public void OnReOpen()
        {
            LoadProducts();
        }

        private void BtnCreateProduct_OnClick(object sender, RoutedEventArgs e)
        {
            ProductForm productForm = new ProductForm(false);
            Globals.OpenDialog(productForm, OnPopUpClose);
        }

        private void BtnCreatePacket_OnClick(object sender, RoutedEventArgs e)
        {
            Globals.MainWindow.CcPrincipal.Content = new ProductForm(true, GetType());
        }

        private void OnPopUpClose(object sender, DialogClosingEventArgs eventargs)
        {
            OnReOpen();
        }
    }
}
